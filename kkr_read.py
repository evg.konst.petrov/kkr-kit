#!/usr/bin/env python3
import sys
import os

# get some arguments, paths, etc
cwd = os.getcwd()
kit_dir = os.path.dirname(os.path.realpath(__file__))

key_args = []   # var for keys like '-efermi=0.123'
dir_args = []   # var for dirnames in which files will be processed

# check if there any keys set and divide them into key_args and dir_args
if len(sys.argv) > 1:
    for arg in sys.argv[1:]:
        if arg[0] == '-':
            key_args.append(arg)
        else:
            dir_args.append(arg)
if len(dir_args) == 0:
    dir_args.append('.')    # if no dirname set, process files in current dir
    if_curr_dir = True
else:
    if_curr_dir = False

# START
from read_func.cell import Cell
from read_func.inp import Inp

# loop over dir args
for d in dir_args:
    os.chdir(cwd)
    dirname = d.replace('/', '_')
    if dirname[-1] == '_':
        dirname = f'{dirname[:-1]}'
    # change dir and print
    if if_curr_dir:
        print('Processing current directory...')
    else:
        print(f'Enter directory:    {d}. Processing...')
        os.chdir(d)     # go to that dir

    # read input
    inp = Inp()
    job = inp.job
    cell = Cell( inp.str_filename )
    out_dirname = f'_data_{cell.name}_job={job}'

    if if_curr_dir:
        out_dir = cwd + f'/{out_dirname}'
    else:
        out_dir = cwd + f'/_data_set_{inp.name}/{out_dirname}'
    try:
        os.mkdir(out_dir)
    except:
        pass

    # call proper script according to job value
    if job in ['301', '201', '101', '001']:
        print('Error! job=x01 is self-consistent calculation. I don\'t know what can i do with it!')

    elif job in ['302']:
        print(f'job = {job} detected. Reading density of states.')
        from read_jobs import job302
        job302.run()

    elif job in ['303']:
        print(f'job = {job} detected. Reading Bloch spectral function.')
        from read_jobs import job303
        job303.run()

    elif job in ['312']:
        print(
            f'job = {job} detected. Reading magnon spectrum and exchange coupling parametes.')
        from read_jobs import job312
        job312.run()

print('Done.')
sys.exit()
