#!/usr/bin/env python3
import sys
sys.dont_write_bytecode = True
print('Welcome to kkr.kit! Have fun.')

# list of possible TOOL options.
possible_tool_args = ['poscar2kkr', 'kpoints2kkr', 'kmesh2d', 'bz', 'cell']

# if one of TOOL-options set, switch to TOOL mode
# switch to READ mode otherwise
if len(sys.argv) > 1:
    for arg in sys.argv[1:]:
        if arg in possible_tool_args:
            print('Switching to TOOL mode.')
            tool_arg = arg
            import kkr_tool
            break
    else:
        print('Switching to READ mode.')
        import kkr_read
# switch to reading mode if no option set
else:
    print('Switching to READ mode.')
    import kkr_read

sys.exit()
