This is KKR Kit -- a package for creating draft figures 
and preparing some input files for Arthur's HUTSEPOT code.

CONTRIBUTORS:
* Evegnii Petrov, Laboratory of Nanostructured Surfaces and Coatings, 
Tomsk State University
* Martin Hoffmann, Institut fuer Theoretische Physik,
Johannes Kepler Universitaet

STRUCTURE

READ POSSIBILITIES: can read and plot
* Bloch spectral function
* density of states
* magnon spectrum

TOOL POSSIBILITIES
* constructing HUTSEPOT structure file from VASP POSCAR file
* constructing HUTSEPOT k-path for band structures from VASP KPOINTS file
* constructing 2d k-meshes

COMING SOON
* reading and plotting exchange interaction parameters
* unit cell and BZ visualization
* MT / WS radii manupulation
* empty spheres positions finder