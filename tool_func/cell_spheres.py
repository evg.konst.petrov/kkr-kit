#!/usr/bin/env python3
import numpy as np
from database.constants import *
from read_func.cell import Cell

class CellSpheres(Cell):
    def __init__(self, filename):
        super().__init__(filename)
        self.which_radii = 'rmtvor'
        self.num_E_to_find = -1
        self.manual_E_indices = []
        self.what_to_do()

        self.trans_vectors = self.get_trans_vectors()
        self.at_pos_dir_trans, self.at_pos_cart_trans = \
            self.get_pos_trans()
        self.get_coo_spheres()
        self.voronoi = self.get_voronoi()
        self.get_rmt_voronoi()
        self.get_rws_voronoi()
        self.num_vor_vertices = 0
        self.v_points = None
        self.v_points_priority = None
        self.do_stuff()

    def what_to_do(self):
        from kkr_tool import key_args
        for arg in key_args:
            if '--radii' in arg:
                if '=' in arg:
                    self.which_radii = arg.split('=')[1]
                else:
                    self.which_radii = 'vasp'
                
                if self.which_radii == 'rmtvor':
                    print('All radii will be set to inscribed Voronoi radii')
                elif self.which_radii == 'rwsvor':
                    print('All radii will be set to WS Voronoi radii (of equal volume)')
                elif self.which_radii == 'rmttouch':
                    print('All radii will be set to touching MT radii (scaled staring from Voronoi MT)')
                elif self.which_radii == 'vasp':
                    print('All _ATOMIC_ radii will be set to the ones from my database so they dont overlap; empty spheres radii will be set to touching radii.')
    
            if '--findE' in arg or '--finde' in arg:
                if '=' in arg:
                    tmp_1 = arg.split('=')[1]
                    if '+' in tmp_1:
                        self.num_E_to_find = int( tmp_1.split('+')[0] )
                        tmp_2 = tmp_1.split('+')[1].split(',')
                        for number in tmp_2:
                            self.manual_E_indices.append(int(number))
                    else:
                        self.num_E_to_find = int(tmp_1)
                else:
                    self.num_E_to_find = 0
                
                if self.num_E_to_find > 0:
                    print(f'I will try to find {self.num_E_to_find} postions for empty spheres.')
                elif self.num_E_to_find < 0:
                    print('You have set --findE option, but requested the negative number of empty spheres to find. Go rest.')
                    self.num_E_to_find = -1

    def get_radii(self):
        if self.which_radii =='rmttouch':
            output_radii = self.get_rmt_touching()
        elif self.which_radii == 'vasp':
            rmt_vasp = self.get_vasp_radii('RWIGS')
            output_radii = self.get_rmt_touching(rmt_vasp)
        elif self.which_radii == 'rwsvor':
            output_radii = [atom.rws_vor for atom in self.atoms]
        elif self.which_radii == 'rmtvor':
            output_radii = [atom.rmt_vor for atom in self.atoms]
        try:
            for i, atom in enumerate(self.atoms):
                atom.output_radius = output_radii[i]
        except:
            print('WARNING!\nYou have not specified which radii you want, so Voronoi MT radii will be drawn.\n')

    def do_stuff(self):
        if self.num_E_to_find >= 0:
            self.get_radii()
            # self.find_empty_positions(self.num_E_to_find)
            self.v_points_rank( self.get_v_points() )
            self.get_empty_positions_cmd()
        self.get_radii()




        


    def get_trans_vectors(self, trans_set=[0,-1,1]):
        tx, ty, tz = np.meshgrid(trans_set, trans_set, trans_set)
        tx, ty, tz = tx.flatten(), ty.flatten(), tz.flatten() 
        trans_vectors = np.array((tx, ty, tz)).T
        return(trans_vectors)

    def get_pos_trans(self, set_atoms_pos = True):
        pos_dir_trans_cell = np.array([]).reshape((0, 3))
        for vec in self.get_trans_vectors():
            pos_dir_trans = self.at_pos_dir + vec
            pos_dir_trans_cell = np.vstack(\
                (pos_dir_trans_cell, pos_dir_trans))
        if set_atoms_pos:
            self.at_pos_dir_trans = pos_dir_trans_cell
            self.at_pos_cart_trans = pos_dir_trans_cell @ self.basis_a
        return(pos_dir_trans_cell, \
             pos_dir_trans_cell @ self.basis_a)

    def get_coo_spheres(self, verb=True):
        '''Get coordination spheres radii'''
        tol_digits = 6
        tol = 1e-6
        max_spheres = 10
        max_r = self.latt_a*4
        from scipy.spatial import cKDTree
        tree = cKDTree(self.at_pos_cart_trans)
        neis_dist, neis_ind = tree.query(self.at_pos_cart_trans, k=100,
                                         distance_upper_bound=max_r)

        if verb:
            print()
            print('Coordination spheres radii are calculated:')
            print(f'Atom    Coo radii (A)')
        
        for i, atom in enumerate(self.atoms):
            coo_atoms_ind_trans = []
            coo_radii = []
            coo_names = []
            n_spheres = 0
            neis_dist[i] = np.array(neis_dist[i]).round(tol_digits)
            j_skip = [0]
            for j in range(1, len(neis_ind[i])):
                if n_spheres >= max_spheres:
                    break
                if j in j_skip:
                    continue
                
                ind = neis_ind[i][j]
                ind_cell = ind % self.N_at
                name = self.at_list[ind_cell]
                dist = neis_dist[i][j]

                if dist not in coo_radii:
                    coo_radii.append(dist)
                    coo_atoms_ind_trans.append(ind)
                    coo_names.append(name)
                    n_spheres += 1
                else:
                    name_in = coo_names[ coo_radii.index(dist) ]

                    same_dist_ind = [ind]
                    same_dist_names_out = [self.at_list[ind % self.N_at]]
                    for j2 in range(j+1, len(neis_ind[i])):
                        if neis_dist[i][j2] == dist:
                            same_dist_ind.append(neis_ind[i][j2])
                            same_dist_names_out.append(self.at_list[neis_ind[i][j2] % self.N_at])
                            j_skip.append(j2)
                        else:
                            break
                    
                    already_used_names = []
                    for j2, name_out in enumerate(same_dist_names_out):
                        if name_out in already_used_names or name_out == name_in:
                            continue
                        else:
                            coo_radii.append(dist)
                            coo_atoms_ind_trans.append(same_dist_ind[j2])
                            coo_names.append(same_dist_names_out[j2])
                            n_spheres += 1
                            already_used_names.append(same_dist_names_out[j2])
                   
            atom.coo_radii = coo_radii
            atom.coo_atoms_ind_trans = coo_atoms_ind_trans
            atom.coo_atoms_ind = np.array(atom.coo_atoms_ind_trans) % self.N_at

            if verb:
                print(f'{atom.name:<3}     {np.round(atom.coo_radii[:8], 3)}')
                names_str = ''.join([ "{:<6}".format(n) for n in coo_names ])
                print(f'         {names_str}')
                print()
        if verb:
            print()
        del(tree)

    def get_voronoi(self, verb = True):
        from scipy.spatial import Voronoi
        vor = Voronoi(self.at_pos_cart_trans)
        if verb:
            print('\nVoronoi polyhdra were constructed.')
        return(vor)

    def get_rws_voronoi(self, verb = True, set_rws = True):
        from scipy.spatial import ConvexHull
        vor = self.voronoi
        vor_vertices = vor.vertices
        vor_regions = vor.regions
        # vor_indicies = vor.point_region[:self.N_at]
        rws_vor_radii = []
        if verb:
            print(f'\nWS radii are obtained for each atominc position via Voronoi volumes.\n')
            print(f'Atom    Vor_V (A^3)    Vor_R (A)    Vor_R (au)')
        for i, atom in enumerate(self.atoms):
            poly = ConvexHull( vor_vertices[vor_regions[ vor.point_region[i] ]] )
            atom.vor_vol = poly.volume
            
            vor_r = (atom.vor_vol/4*3/np.pi)**(1/3)
            rws_vor_radii.append(vor_r)
            atom.rws_vor = vor_r
            if verb:
                print(f'{atom.name:<3}     {round(atom.vor_vol, 6):<9}      {round(vor_r, 6):<8}     {round(vor_r/0.5291772083, 6):<8}')
        if verb:
            print('')
        return(np.array(rws_vor_radii))

    def get_rmt_voronoi(self, verb = True):
        from scipy.spatial import ConvexHull
        vor = self.voronoi
        vor_vertices = vor.vertices
        vor_regions = vor.regions
        vor_indicies = vor.point_region[:self.N_at]
        if verb:
            print(f'\nMT radii are obtained for each atominc position via Voronoi polyhedra.\n')
            print(f'Atom    Vor_rmt (A)    Vor_rmt (au)')
        rmt = []
        for i, atom in enumerate(self.atoms):
            poly = ConvexHull( vor_vertices[vor_regions[ vor_indicies[i] ]] )
            eqs = poly.equations
            
            radii = np.array( np.abs(
                [ ( np.sum(eq[:3]*atom.pos_cart) + eq[3] ) / \
                    np.linalg.norm(eq[:3]) for eq in eqs ] ) )
            rmt_vor = np.min(radii)
            atom.rmt_vor = rmt_vor
            rmt.append(rmt_vor)
            if verb:
                print(f'{atom.name:<3}      {round(rmt_vor, 6):<8}     {round(rmt_vor/0.5291772083, 6):<8}')
        if verb:
            print('')
        del(vor)
        return(np.array(rmt))

    def get_vasp_radii(self, mode='RWIGS'):
        from database.at_radii import RWIGS, RCORE, RDEP
        rmt = []
        for i, atom in enumerate(self.atoms):
            if atom.name != 'E':
                if mode == 'RWIGS':
                    rmt.append(RWIGS[atom.name])
                if mode == 'RDEP':
                    rmt.append(RDEP[atom.name])
            else:
                rmt.append(-1.0)
        return(rmt)

    def get_rmt_touching(self, rmt=[], verb=True, set_radii=True):
        fixed_radii = []
        if rmt == []:
            rmt = [atom.rmt_vor for atom in self.atoms]
        
        for i, atom in enumerate(self.atoms):
            if rmt[i] > 0 :
                fixed_radii.append(i)
        rmt = np.array(rmt)
        
        if verb:
            print()
            print('Supplied radii:')
            print(rmt)

            print(f'\nFollowing radii will be kept fixed:')
            print(fixed_radii)
            print()

        cell_overlaps = self.get_overlaps(False, rmt, 6)
        max_overlaps = np.max(cell_overlaps, axis=1)

        while np.max(max_overlaps) > 0:
            rmt *= 0.99
            cell_overlaps = self.get_overlaps(False, rmt, 10)
            max_overlaps = np.max(cell_overlaps, axis=1)
            if verb:
                print(f'Given radii already overlapping!\n Desreasing all radii by 1 %.')
        if verb:
            if np.max(max_overlaps) <= 0:
                print('Given radii not overlapping, proceeding ...')

        tol = 1e-6
        rmt_rescaled = rmt[:]
        for i, atom in enumerate(self.atoms):
            if i not in fixed_radii and rmt[i] <= 0.0:
                rmt_rescaled[i] = atom.rmt_vor*0.5

        if verb:
            print('Staring from following radii:')
            print(rmt_rescaled.round(5).T, '\n')

        cell_overlaps = self.get_overlaps(True, rmt_rescaled, 4)
        max_overlaps = np.max(cell_overlaps, axis=1)
        tot_overlap = np.min(max_overlaps)
        if verb:
            if np.max(max_overlaps) > 0:
                print('Starting radii overlapping!')
            else:
                print('Starting radii are not overlapping, proceed ...')
        
        while tot_overlap < -tol:
            dr = []
            ddr = []
            for i, atom in enumerate(self.atoms):
                if np.abs(max_overlaps[i]) > tol and i not in fixed_radii:
                    max_overlap_nei = np.argmax(cell_overlaps[i])
                    max_overlap_nei_index = atom.coo_atoms_ind[max_overlap_nei]

                    coo_radius = atom.coo_radii[max_overlap_nei]
                    dr.append(coo_radius - rmt_rescaled[i] - rmt_rescaled[max_overlap_nei_index])
                    # ddr.append(1 - cell_overlaps[i].max()/2)
                else:
                    dr.append(0)
                    ddr.append(1.0)
            dr = np.array(dr)
            ddr = np.array(ddr)
            rmt_rescaled += dr*0.5 - tol/10
            # rmt_rescaled *= ddr
            if np.sum(np.abs(dr)) < tol/1000:
                if verb:
                    print('\nMT radii rescaling did not converge... stopping now.')
                    print(f'Mininmal 1st nei overlap was: {tot_overlap} \n')
                break
            cell_overlaps = self.get_overlaps(False, rmt_rescaled, 5)
            max_overlaps = np.max(cell_overlaps, axis=1)
            tot_overlap = np.min(max_overlaps)
            print('Tot overlap: ', tot_overlap)
            # sleep(0.3)
            # print(dr)


        cell_overlaps = self.get_overlaps(verb, rmt_rescaled, 4)
        if set_radii:
            for i, atom in enumerate(self.atoms):
                atom.rmt = rmt_rescaled[i]
        if verb:
            print(f'\nMT radii (touching) are obtained for each atomic position.')
            print(f'Atom    Vor_rmt (A)    Vor_rmt (au)')
            for rmt, atom in zip(rmt_rescaled, self.atoms):
                print(f'{atom.name:<3}      {round(rmt, 6):<8}     {round(rmt*au, 6):<8}')
            print('')
        return(rmt_rescaled)

    def get_rws_from_rmt(self, verb=True, rmt_radii=[],  set_radii=True):
        if rmt_radii == []:
            rmt_radii = np.array([atom.rmt_vor for atom in self.atoms])
        Vc = self.cell_volume
        rws_radii = np.array(rmt_radii)
        Vrws = np.sum( [4*np.pi/3*r**3 for r in rws_radii] )
        dV = Vrws/Vc
        while np.abs(dV - 1) >= 1e-15 :
            dV = Vrws/Vc
            rws_radii /= dV**0.333
            Vrws = np.sum( [4*np.pi/3*r**3 for r in rws_radii] )
        if set_radii:
            if verb:
                print('WS radii were obtained from MT radii (just scaled)')
                print(f'Cell volume: {Vc}, Sum of spheres volumes: {Vrws}')
                print(f'Atom      rws (A)      rws (au)')
            for i, atom in enumerate(self.atoms):
                atom.rws = rws_radii[i]
                if verb:
                    print(f'{atom.name:<3}      {round(rws_radii[i], 6):<8}     {round(rws_radii[i]/0.5291772083, 6):<8}')
        return(rws_radii)

    def get_rmt_remaining(self, verb=True, rmt_radii=[], set_radii=True):
            if rmt_radii == []:
                print('No radii set, so ALL WS-radii will be calculated.')
                rmt_radii = [atom.rmt_vor for atom in self.atoms]
            else:
                dn = self.N_at - len(rmt_radii)
                print(f'Last {dn} radii were not set, so they are assumed to be 0.')
                rmt_radii = list(rmt_radii)
                rmt_radii += [0.0] * dn

            first_coo_radii = np.array( [atom.coo_radii[0] for atom in self.atoms] )
            first_nei_ind = [atom.coo_atoms_ind[0] for atom in self.atoms]
            first_coo_radii_sort = np.argsort(first_coo_radii)
            for i in first_coo_radii_sort[::-1]:
                if rmt_radii[i] > 0.0:
                    continue
                if rmt_radii[first_nei_ind[i]] != 0:
                    rmt_radii[i] = first_coo_radii[i] - rmt_radii[first_nei_ind[i]] 
                else:
                    rmt_radii[i] = self.atoms[i].rmt_vor
            return(rmt_radii)

    def get_rws_remaining(self, verb=True, rws_radii=[], set_radii=True):
        if rws_radii == []:
            print('No radii set, so ALL WS-radii will be calculated.')
            return(self.get_rws_voronoi())
        elif len(rws_radii) != self.N_at:
            dn = self.N_at - len(rws_radii)
            print(f'Last {dn} radii were not set, so they are assumed to be 0.')
            while len(rws_radii) != self.N_at:
                rws_radii.append(0.0)


        # else:
            at_ind = []
            for i, atom in enumerate(self.atoms):
                if rws_radii[i] <= 0.0:
                    at_ind.append(i)
                    rws_radii[i] = atom.rmt_vor
                    # print( atom.rmt_vor)
            Vc = self.cell_volume


            if verb:
                print(f'Following atoms radii will be calculated: {at_ind}. \nOthers will be kept fixed.')
            
            rws_radii = np.array(rws_radii)
            Vrws = np.sum( [4*np.pi/3*r**3 for r in rws_radii] )
            dV = Vrws / Vc
            print(dV)
            while np.abs(dV - 1) > 1e-15 :
                
                # print(rws_radii)
                rws_radii[at_ind] /= (dV**0.333333)
                Vrws = np.sum( [4*np.pi/3*r**3 for r in rws_radii] )
                dV = Vrws / Vc
                print('dV = ', dV)
                break
            
            if set_radii:
                if verb:
                    print('Remaining WS radii were calculated (just scaled)')
                    print(f'Cell volume: {Vc}, Sum of spheres volumes: {Vrws}')
                    print(f'Atom      rws (A)      rws (au)')
                for i, atom in enumerate(self.atoms):
                    atom.rws = rws_radii[i]
                    if verb:
                        print(f'{atom.name:<3}      {round(rws_radii[i], 6):<8}     {round(rws_radii[i]/0.5291772083, 6):<8}')
            return(rws_radii)

    def get_overlaps(self, verb=True, radii=[], nspheres=6):
        if radii == []:
            # print('No RWS radii supplied, so using atom.rws')
            radii = [atom.rws_vor for atom in self.atoms]
        
        if verb:
            print('Radii overlaps calculated.')
            print(f'Atom    Overlaps with closest neighbours')
        overlaps_cell = []
        for i, atom in enumerate(self.atoms):
            overlaps = []
            for j in range(nspheres):
                nei_ind = atom.coo_atoms_ind[j]
                nei_dist = atom.coo_radii[j]
                nei_r = radii[nei_ind]
                overlaps.append( (radii[i] + nei_r - nei_dist) / nei_dist )
            overlaps_cell.append(overlaps)
            if verb:
                atom.coo_overlaps = overlaps
                overlaps_string = ['{: .2f}'.format(overlap) for overlap in overlaps]
                overlaps_names = [self.atoms[j].name for j in atom.coo_atoms_ind]
                output_string = ''
                for j in range(len(overlaps_string)):

                    output_string += f'{overlaps_string[j]}: {overlaps_names[j]:<3}  '
                    #' '.join(map(str, zip(overlaps_string, overlaps_names)))
                if verb:
                    print(f'{i:<3} {atom.name:<3}  {output_string}')
        return(np.array(overlaps_cell))

    def cell_overlap(self, rws_radii=[], verb=False):
        overlaps = np.array(self.get_overlaps(rws_radii))
        overlaps_1 = overlaps[:,0]
        if verb:
            print(f'Max 1st overlap: {np.max(overlaps_1)}')
            print(f'Mean 1st overlap: {np.mean(overlaps_1)}')
            print(f'RMS 1st overlap: {np.mean(overlaps_1**2)**0.5}')
        positive_overlaps = (overlaps + np.abs(overlaps))/2
        cell_overlaps = np.mean(positive_overlaps**2)**0.5
        return(cell_overlaps)

    def optimize_rws(self, rws_radii=[], verb=True, set_radii=True):
        if rws_radii == []:
            rws_radii = [atom.rws_vor for atom in self.atoms]
        tol = 0.18
        rws_radii_rescaled = np.array(rws_radii)

        cell_overlaps = self.get_overlaps(False, rws_radii , 3)
        max_overlaps = np.max(cell_overlaps, axis=1)
        min_overlaps = np.min(cell_overlaps, axis=1)
        max_cr = np.max(max_overlaps)
        min_cr = np.min(cell_overlaps)
        while max_cr > tol:
            atoms_to_rescale = []
            factors = []
            for i, atom in enumerate(self.atoms):
                if max_overlaps[i] > tol :
                    atoms_to_rescale.append(i)
                    if atom.name != 'E':
                        d = 0.5
                    else:
                        d = 1
                    factors.append(1 - (max_overlaps[i] - tol)*d/2 )
                else:
                    factors.append(1)
            rws_radii_rescaled *= np.array(factors)
            rws_radii_rescaled = self.get_rws_from_rmt(False, rws_radii_rescaled )

            cell_overlaps = self.get_overlaps(False, rws_radii_rescaled, 3)
            max_overlaps = np.max(cell_overlaps, axis=1)
            min_overlaps = np.min(cell_overlaps, axis=1)
            max_cr = np.max(max_overlaps)
            min_cr = np.min(cell_overlaps)
            # print('Max overlap: ', max_cr, 'Min overlap: ', min_cr)
            # time.sleep(0.2)

        cell_overlaps = self.get_overlaps(verb, rws_radii_rescaled, 3)
        
        if set_radii:
            for i, atom in enumerate(self.atoms):
                atom.rws = rws_radii_rescaled[i]
        if verb:
            print(f'\nOptimized WS radii are obtained for each atomic position.\n')
            print(f'Atom     rws (A)     rws (au)')
            for rws, atom in zip(rws_radii_rescaled, self.atoms):
                print(f'{atom.name:<3}      {round(rws, 6):<8}     {round(rws/0.5291772083, 6):<8}')
        return(rws_radii_rescaled)

    def get_packing_coeff(self, verb=True, rmt_radii=[]):
        if rmt_radii == []:
            rmt_radii = self.get_rmt_touching(False, [], False)
        sum_V_spheres = np.sum([4/3*np.pi*r**3 for r in rmt_radii])
        coeff = sum_V_spheres / self.cell_volume
        if verb:
            print(f'Close packing coefficient = {coeff}')
        return( coeff )

    def get_v_points_all(self, return_index=False, verb=True, clusterize=True):
        vor = self.voronoi
        from scipy.spatial import ConvexHull
        v_points = vor.vertices[:]
        vor_regions = vor.regions
        vor_indicies = vor.point_region
        self.num_vor_vertices = len(v_points)
        # adding points in simplexes centers
        planes_points = []
        for j in range(len(self.at_pos_cart_trans)):
            try:
                poly = ConvexHull( v_points[vor_regions[ vor_indicies[j] ]] )
            except:
                print(f'Building Voronoi polyhedron # {j} / {len(self.at_pos_cart_trans)} failed. Proceeding...')
                continue

            at_pos = self.at_pos_cart_trans[j]
            eqs = poly.equations
            for eq in eqs:
                d = np.abs(
                    (eq[0]*at_pos[0] + eq[1]*at_pos[1] + eq[2]*at_pos[2] + eq[3])) \
                    / np.sum(eq[:3]**2)**0.5
                vec = np.array([eq[0], eq[1], eq[2]])*d
                planes_points.append(at_pos + vec)

        # vertices points are unique, but planes points have a lot of repetitions
        v_points = np.concatenate((v_points, planes_points), axis=0)
        _, unique_index = np.unique(v_points.round(12), return_index=True, axis=0)
        # but i have to preserbe the order
        v_points = v_points[ np.sort(unique_index) ]

        return(v_points)

    def v_points_assign(self, v_points, verb=False):
        v_points_assignment = [[]]*len(v_points) # each = list of indices of traslated positions
        # assign voronoi vertices via vorooi polyhedra
        vor = self.voronoi
        vor_regions = vor.regions
        vor_indicies = vor.point_region

        for i, at_pos in enumerate(self.at_pos_cart_trans):
            for index in vor_regions[ vor_indicies[i] ]:
                if index != -1:
                    v_points_assignment[index] = v_points_assignment[index] + [i]
        planes_points = v_points[self.num_vor_vertices:]
        from scipy.spatial import cKDTree
        atoms_and_planes_points = np.concatenate((self.at_pos_cart_trans, planes_points))
        tree = cKDTree(atoms_and_planes_points)
        neis_dist, neis_ind = tree.query(atoms_and_planes_points, k=25)
        # assign voronoi plane points to two closest atoms
        for i, p in enumerate(planes_points):
            index = i + self.num_vor_vertices
            num_at_assigned = 0
            for index_tree in neis_ind[i + len(self.at_pos_cart_trans)][1:]:
                if num_at_assigned > 1:
                    break
                if index_tree >= len(self.at_pos_cart_trans):
                    continue
                v_points_assignment[index] = v_points_assignment[index] + [index_tree]
                num_at_assigned += 1
        if verb:
            for i, vpa in enumerate(v_points_assignment):
                print(f'V_point #{i:>5}: atoms: {vpa}')
        return(v_points_assignment)

    def v_points_optimize(self, v_points, v_points_assignment):
        # from database.at_radii import RWIGS
        for i, p in enumerate(v_points):
            radii = []
            distances = []
            vectors = []
            for j, nei in enumerate(v_points_assignment[i]):
                index = nei % self.N_at
                atom_name = self.atoms[index].name
                if atom_name != 'E':
                    radius = self.atoms[index].rmt
                else:
                    radius = self.atoms[index].rmt
                radii.append(radius)
                vector = np.array( self.at_pos_cart_trans[nei] - p )
                vectors.append( vector / np.linalg.norm(vector) )
                distances.append( np.linalg.norm(vector) )
            radii = np.array(radii)
            distances = np.array(distances)
            poly_points = []
            for r, v, d in zip(radii, vectors, distances):
                poly_points.append(v_points[i] + v*(d-r))
            if len(poly_points) > 0:
                v_points[i] = np.mean(poly_points, axis=0)
            else:
                pass
        return(v_points)

    def v_points_clusterize(self, v_points, verb=True):
        vor_vert_cluster_tol = np.mean([atom.coo_radii[0] for atom in self.atoms])/2

        v_points_cls = []
        from scipy.spatial import cKDTree
        tree = cKDTree(v_points)
        neis_dist, neis_ind = tree.query(v_points, k=100,
                                distance_upper_bound=self.latt_a*2)

        if verb:
            print(f'Voronoi vertices cluster tolerance: {vor_vert_cluster_tol}')
            print('Searching for voronoi vertices clusters...')

        v_points_to_preserve = []
        v_points_clusterized = []
        neis_num_nei = []
        
        for d in neis_dist:
            r1 = d[1]
            nr1 = 1
            for r in d[2:]:
                if np.abs(r1 - r) < 1e-3:
                    nr1 += 1
                else:
                    break
            neis_num_nei.append(nr1)
        neis_num_nei_sort = np.argsort(neis_num_nei)

        # fnd_sort_ind = np.argsort([d[1] for d in neis_dist])
        for i in neis_num_nei_sort[::-1]:
            if i in v_points_clusterized or i in v_points_to_preserve:
                continue

            r1 = neis_dist[i][1]

            if r1 > vor_vert_cluster_tol:
                v_points_to_preserve.append(i)
                continue
            
            # nr1 = 0
            for j, r in zip(neis_ind[i][1:], neis_dist[i][1:]):
                if np.abs(r - r1) < 1e-3:
                    # nr1 += 1
                    v_points_clusterized.append(j)
                else:
                    break
            v_points_to_preserve.append(i)
            
        v_points_cls = v_points[v_points_to_preserve]

        # already_clusterized = []
        # for i in range(len(v_points)):
        #     if i in already_clusterized:
        #         continue
        #     already_clusterized.append(i)
        #     current_cluster_ind = [i]
        #     current_cluster_vertices = [v_points[i]]
        #     for j in range(1, len(neis_dist[i][1:])):
        #         if neis_dist[i][j] > vor_vert_cluster_tol:
        #             break
        #         already_clusterized.append(neis_ind[i][j])
        #         current_cluster_ind.append(neis_ind[i][j])
        #         current_cluster_vertices.append(v_points[neis_ind[i][j]])
        #     for k in current_cluster_ind[1:]:
        #         for j in range(1, len(neis_dist[k][1:])):
        #             if neis_dist[k][j] > vor_vert_cluster_tol/2:
        #                 break
        #             if neis_ind[k][j] in already_clusterized:
        #                 continue
        #             already_clusterized.append(neis_ind[k][j])
        #             current_cluster_ind.append(neis_ind[k][j])
        #             current_cluster_vertices.append(v_points[neis_ind[k][j]])
        #     current_cluster_vertices = np.array(current_cluster_vertices)
        #     v_points_cls.append(np.mean(current_cluster_vertices, axis=0))
            # print(np.mean(current_cluster_vertices, axis=0))
        
        return(v_points_cls)

    def v_points_truncate(self, v_points):
        v_points_dir = v_points @ np.linalg.inv(self.basis_a)
        v_points_in = []
        for p in v_points_dir:
            if all(p >= 0.0) and all(p <= 1.0):
                v_points_in.append(p)
        return(v_points_in @ self.basis_a)

    def v_points_rank_0(self, v_points):
        # from database.at_radii import RWIGS
        points = np.concatenate((self.at_pos_cart_trans, v_points), axis=0)
        N_at_trans = len(self.at_pos_cart_trans)
        from scipy.spatial import cKDTree
        tree = cKDTree(points)
        neis_dist, neis_ind = tree.query(points, k=50, distance_upper_bound=self.latt_a*4)
        v_points_priority = []
        
        for i in range(N_at_trans, len(points)):
            coo_r_1 = 0.0
            tmp = []
            for d, ind in zip(neis_dist[i][:], neis_ind[i][:]):
                if ind >= N_at_trans:
                    continue
                atom  = self.atoms[ ind % self.N_at ]
                if atom.name != 'E':
                    r = atom.rmt
                else:
                    r = atom.rws_vor
                if d/r < 1.0:
                    d = 0
                if coo_r_1 == 0.0:
                    coo_r_1 = d
                elif coo_r_1 > 0.0 and np.abs(d - coo_r_1) > 1e-2:
                    break
                if d > self.latt_a:
                    factor = 1
                # elif d > 0:
                    # factor = 1/d
                else:
                    factor = 1
                tmp.append((d - r)/r)
            if len(tmp) > 0:
                if any([t <= 0.0 for t in tmp]):
                    v_points_priority.append(0.0)
                    pass
                else:
                    v_points_priority.append(np.prod(np.array(tmp))*np.max(tmp)/np.min(tmp))
            else:
                v_points_priority.append(0.0)

        v_points_priority = np.clip(np.array(v_points_priority), 0.0, np.max(v_points_priority))
        v_points_priority = v_points_priority / np.max(v_points_priority) + 0.1
        # self.v_points_priority = v_points_priority
        return(v_points_priority)

    def v_points_rank(self, v_points):
        # from database.at_radii import RWIGS
        points = np.concatenate((self.at_pos_cart_trans, v_points), axis=0)
        N_at_trans = len(self.at_pos_cart_trans)
        v_points_priority = np.zeros(len(v_points))
        from scipy.spatial import cKDTree
        tree = cKDTree(points)
        neis_dist, neis_ind = tree.query(points, k=100,
                                distance_upper_bound=self.latt_a*2)

        for i in range(len(v_points)):
            index = i + N_at_trans
            tmp = []
            for j, d in zip(neis_ind[index][1:], neis_dist[index][1:]):
                if j >= N_at_trans:
                    continue
                else:
                    r = self.atoms[j % self.N_at].rmt
                    tmp.append(d-r)
            tmp = np.sort( np.unique(tmp) )
            v_points_priority[i] = (tmp[0] + tmp[1])/2


        v_points_priority = np.clip(np.array(v_points_priority), 0.0, np.max(v_points_priority))
        # v_points_priority = v_points_priority / np.max(v_points_priority) + 0.1
        self.v_points_priority = v_points_priority
        return(v_points_priority)


    def get_v_points(self):
        v_points = self.get_v_points_all()
        v_points = self.v_points_optimize(v_points, self.v_points_assign(v_points, False))
        # v_points = self.v_points_clusterize(v_points)
        v_points = self.v_points_truncate(v_points)
        self.v_points = v_points
        print(f'Total number of v_points: {v_points.shape}')
        return(v_points)

    def get_v_points_rad(self, verb=True, clusterize=True, rmt_radii = []):
        if rmt_radii == []:
            rmt_radii = [atom.rmt_vor for atom in self.atoms]
        rmt_radii = np.array(rmt_radii)
        rws_radii = np.array(self.get_rws_from_rmt(False, rmt_radii, False))
        radii = (rmt_radii + rws_radii)/2
        print('Constructing angular grids...')
        points = self.at_pos_cart_trans[:]
        n_ang = 10
        # n_rad = 10

        thetas, phis = np.meshgrid(
                np.linspace(0, np.pi, n_ang, endpoint=False), 
                np.linspace(0, np.pi*2, n_ang*2, endpoint=False) )
        thetas, phis = phis.flat, thetas.flat

        for i, pos in enumerate(self.at_pos_cart_trans):
            at_index = i % self.N_at
            r = rmt_radii[at_index]
            dx = r*np.sin(thetas)*np.cos(phis)
            dy = r*np.sin(thetas)*np.sin(phis)
            dz = r*np.cos(thetas)
            points = np.vstack( (points, np.array((dx,dy,dz)).T + pos) )

        print('Constructing voronoi polyhedra on angular grids...')
        from scipy.spatial import ConvexHull, Voronoi
        vor = Voronoi(points)
        vor_vertices = vor.vertices[:]
        # vor_regions = vor.regions
        # vor_indicies = vor.point_region
    
        # print('Adding points in voronoi simplices centers...')
        # planes_points = []
        # for i, atom in enumerate(self.atoms):
        #     poly = ConvexHull( vor_vertices[vor_regions[ vor_indicies[i] ]] )
        #     eqs = poly.equations
        #     for eq in eqs:
        #         d = np.abs(
        #             (eq[0]*atom.pos_cart[0] + eq[1]*atom.pos_cart[1] + eq[2]*atom.pos_cart[2] + eq[3]) \
        #             / np.sum(eq[:3]**2) 
        #             )
        #         vec = np.array([eq[0], eq[1], eq[2]])*d
        #         planes_points.append(atom.pos_cart + vec)
        # vor_vertices = np.concatenate((vor_vertices, planes_points), axis=0)
        # tmp, unique_index = np.unique(vor_vertices.round(10), return_index=True, axis=0)
        # vor_vertices = vor_vertices[ unique_index ]

        print('Removing voronoi vertices inside WS spheres...')
        dist_field = []
        for vert in vor_vertices:
            distances = np.sum( (self.at_pos_cart_trans - vert)**2, axis=1)**0.5
            dmin_index = np.argmin(distances)
            dmin = distances[dmin_index]
            at_index = dmin_index % self.N_at
            dist_field.append( dmin - radii[at_index] )

        dist_field = np.array(dist_field)
        
        which_outside = np.argwhere( dist_field > 1e-3).T[0]
        dist_field = dist_field[which_outside]
        vor_vertices = vor_vertices[which_outside]
        print(dist_field.shape)

        # vor_vertices = vor_vertices[:]

        print(f'Number of voronoi vertices outside WS spheres: {vor_vertices.shape}')

        # vor_vert_cluster_tol = np.max([atom.coo_radii[0] for atom in self.atoms]) / 5
        # # vor_vert_cluster_tol = cell.latt_a/10
        # if verb:
        #     print(f'Total number of Voronoi vertices: {len(vor_vertices)}')
        #     print(f'Voronoi vertices cluster tolerance: {vor_vert_cluster_tol}')
        #     print('Searching for voronoi vertices clusters...')

        # if clusterize:
        #     vor_vertices_cls = []
        #     from scipy.spatial import cKDTree
        #     tree = cKDTree(vor_vertices)
        #     neis_dist, neis_ind = tree.query(vor_vertices, k=100,
        #                             distance_upper_bound=self.latt_a*2)
        #     already_clusterized = []
        #     for i in range(len(vor_vertices)):
        #         print(i)
        #         if i in already_clusterized:
        #             continue
        #         already_clusterized.append(i)
        #         current_cluster_ind = [i]
        #         current_cluster_vertices = [vor_vertices[i]]
        #         for j in range(1, len(neis_dist[i][1:])):
        #             if neis_dist[i][j] > vor_vert_cluster_tol:
        #                 break
        #             already_clusterized.append(neis_ind[i][j])
        #             current_cluster_ind.append(neis_ind[i][j])
        #             current_cluster_vertices.append(vor_vertices[neis_ind[i][j]])
        #         for k in current_cluster_ind[1:]:
        #             for j in range(1, len(neis_dist[k][1:])):
        #                 if neis_dist[k][j] > vor_vert_cluster_tol/2:
        #                     break
        #                 if neis_ind[k][j] in already_clusterized:
        #                     continue
        #                 already_clusterized.append(neis_ind[k][j])
        #                 current_cluster_ind.append(neis_ind[k][j])
        #                 current_cluster_vertices.append(vor_vertices[neis_ind[k][j]])
        #         current_cluster_vertices = np.array(current_cluster_vertices)
        #         vor_vertices_cls.append(np.mean(current_cluster_vertices, axis=0))
        #         # print(np.mean(current_cluster_vertices, axis=0))
        
        # else: # if not clusterize
        #     vor_vertices_cls = vor_vertices[:]
        
        v_points_in = []
        dist_field_in = []
        vor_vertices_cls = np.array(vor_vertices)
        for i, vert in enumerate(vor_vertices_cls):
            vert_dir = np.round((vert @ np.linalg.inv(self.basis_a)), 10) + 0
            if all(c >= 0.0 for c in vert_dir) and all(c < 1.0 for c in vert_dir):
                v_points_in.append(vert)
                dist_field_in.append(dist_field[i])

        v_points_in = np.array(v_points_in)
        dist_field_in = np.array(dist_field_in)
        if verb:    
            print(f'Voronoi vertices clusterized into {len(v_points_in)} points.')
        return(v_points_in, dist_field_in**2)


    def find_empty_positions(self, num_E_to_find):
        # v_points = self.get_v_points()
        
        for n in range(num_E_to_find):
            self.get_radii()
            v_points = self.get_v_points()
            v_points_priority = self.v_points_rank(v_points)
            best_index = np.argsort(v_points_priority)[::-1]
            best_point_dir = v_points[best_index[0]] @ np.linalg.inv(self.basis_a)
            self.add_empty_sphere(best_point_dir)
            # v_points_priority = self.v_points_rank(v_points)
        for index in self.manual_E_indices:
            if self.num_E_to_find == 0:
                v_points = self.get_v_points()
            self.add_empty_sphere(v_points[index] @ np.linalg.inv(self.basis_a) )
        # self.v_points_priority = self.v_points_rank(v_points)

    def get_empty_positions_cmd(self):
        input_str = ''
        from tool_func.draw_cell import DrawCell
        draw = DrawCell(self)
        
        while True:
            input_str = input('Enter position index: ')
            if input_str.strip() == 'done':
                break
            if input_str in ['update', 'upd']:
                self.v_points = self.get_v_points()
                self.v_points_priority = self.v_points_rank(self.v_points)
            else:
                # try:
                index = int(input_str)
                self.add_empty_sphere(self.v_points[index] @ np.linalg.inv(self.basis_a) )
            draw.show_update()
                # except:
                    # print('Error. Try that again.')


    def add_empty_sphere(self, pos_dir):
        from read_func.cell import Atom
        self.at_list += ['E']
        self.N_at += 1
        self.at_pos_dir = np.vstack((self.at_pos_dir, pos_dir))
        self.at_pos_cart = np.vstack((self.at_pos_cart, pos_dir @ self.basis_a))
        self.atoms.append(Atom(self, -1))

        # reinit
        self.at_pos_dir_trans, self.at_pos_cart_trans = self.get_pos_trans()
        self.get_coo_spheres(False)
        self.voronoi = self.get_voronoi(False)
        self.get_rmt_voronoi(False)
        self.get_rws_voronoi(False)
        # self.num_vor_vertices = 0

        print('Inserted empty sphere at:')
        print(np.array2string(pos_dir, formatter={'float': '{:0.14f}'.format}).replace('[',' ').replace(']',' ') + '\n' )
        # print(f'Now there are {len(self.atoms)} atoms.')


    def optimize_empty_positions(self):
        empty_indices = [i  for i, atom in enumerate(self.atoms) if atom.name == 'E']
        rmt_atoms  = [atom.rmt  for i, atom in enumerate(self.atoms) if atom.name != 'E']
        empty_forces = np.zeros((len(empty_indices), 3))
        max_force = 1

        from time import sleep

        while max_force > 1e-10:
            empty_forces *= 0
            rmt = self.get_rmt_touching(False, rmt_atoms)
            print(rmt)
            print('Atomic radii:')
            print(rmt_atoms)
            print('Empty radii:')
            print(rmt[len(rmt_atoms):])
            sleep(0.1)
            for i, empty_index in enumerate(empty_indices):
                # vecs = self.at_pos_cart[empty_index] - self.at_pos_cart_trans
                _, at_pos_cart_trans_big = self.get_pos_trans_big()
                vecs = self.at_pos_cart[empty_index] - at_pos_cart_trans_big
                vecs_l = np.sum( vecs**2, axis=1 )**0.5
                js = np.argwhere(1e-3 < vecs_l ).flatten()#> self.latt_a*0.99)
                jss = np.argwhere( self.latt_a*1 >= vecs_l[js] ).flatten()
                js = js[jss]
                indices = js % self.N_at

                for j, index in zip(js, indices):
                    evector = vecs[j] / vecs_l[j]
                    # df = (rmt[index] + rmt[i]*0)/vecs_l[j]
                    df = (vecs_l[j] - rmt[index])/(vecs_l[j]**3)
                    f = evector * df
                    empty_forces[i] -= f
            # 
            empty_forces += 0 #np.round(empty_forces, 14) + 0
            print(empty_forces)
            # sleep(0.1)
            max_force = np.abs(np.max( np.sum(empty_forces**2, axis=1) ))**0.5
            # print(max_force)
            disp = empty_forces / 2

            self.at_pos_cart[empty_indices] += disp
            self.at_pos_dir[empty_indices] += disp @ np.linalg.inv(self.basis_a)
            for i in empty_indices:
                self.atoms[empty_index] = Atom(self, empty_index)
            self.at_pos_dir_trans, self.at_pos_cart_trans = self.get_pos_trans()
            self.get_coo_spheres(False)
            # self.reinit()
        print('Done')
        self.reinit()
        
