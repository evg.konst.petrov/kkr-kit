#!/usr/bin/env python3

from read_func.func import *

import mayavi.mlab as mv
import numpy as np
import os
os.environ['QT_API'] = 'pyqt'
os.environ['ETS_TOOLKIT'] = 'qt4'

plot = True


def get_rec_basis(target_str='file.str'):
    f = open(target_str, 'r')
    file = f.readlines()
    basis_lines = file[4:7]

    for i in range(len(basis_lines)):
        basis_lines[i] = basis_lines[i].replace('=', ' ')
        basis = np.loadtxt(basis_lines, usecols=(-5, -4, -3))
        a, b, c = float(get_val('alat', file)), \
            float(get_val('blat', file)), \
            float(get_val('clat', file))
        print(a, b, c)
        basis = basis * np.array([a, b, c]) / a
        rec_basis = np.linalg.inv(basis.T)

    return(rec_basis)


def make_rec_nodes(rec_basis):
    nodes_int = [[0, 0, 0]]
    for i in range(-1, 2):
        for j in range(-1, 2):
            for k in range(-1, 2):
                if (i, j, k) != (0, 0, 0):
                    nodes_int.append([i, j, k])
    nodes_int = np.array(nodes_int)
    nodes = nodes_int @ rec_basis
    return(nodes)


def draw_arrows(rec_basis):
    vectors = [
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1]]
    arr_colors = [(1, 0, 0), (0, 1, 0), (0, 0, 1)]
    for i, v in enumerate(vectors):
        vec = np.array(v) @ rec_basis
        print(vec)
        mv.quiver3d(*[*[0, 0, 0], *vec],
                    resolution=32, scale_factor=1,
                    color=arr_colors[i],
                    line_width=2)


def get_voronoi(nodes):
    from scipy.spatial import ConvexHull, Voronoi
    vor = Voronoi(nodes)
    vor_vertices = vor.vertices
    vor_regions = vor.regions
    vor_indicies = vor.point_region
    poly = ConvexHull(vor_vertices[vor_regions[vor_indicies[0]]])
    return(poly)


def draw_voronoi(nodes):
    from scipy.spatial import ConvexHull, Voronoi
    vor = Voronoi(nodes)
    vor_vertices = vor.vertices
    vor_regions = vor.regions
    vor_indicies = vor.point_region
    # poly = ConvexHull(vor_vertices[vor_regions[vor_indicies[0]]])
    ridges = vor.ridge_vertices
    # ridge_points = vor.vertices[ridges]
    for r in ridges:
        p1, p2 = vor.vertices[r[0]], vor.vertices[r[1]]
        pass
        mv.plot3d([p1[0], p2[0]], [p1[1], p2[1]], [p1[2], p2[2]],
                  color=(0, 0, 0))


def run():
    rec_basis = get_rec_basis()
    nodes = make_rec_nodes(rec_basis)

    if plot:
        for node in nodes:
            mv.points3d(*[*node, 0.1],
                        resolution=32, scale_factor=1,
                        color=(1, 0, 0))

        # vor = get_voronoi(nodes)
        # for v in vor.points:
        #     mv.points3d(*[*v, 0.1],
        #                 resolution=32, scale_factor=1,
        #                 color=(0.5, 0.5, 0.5))
        draw_arrows(rec_basis)
        draw_voronoi(nodes)
        mv.gcf().scene.parallel_projection = True
        mv.show()
