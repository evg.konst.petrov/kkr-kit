#!/usr/bin/env python3
import numpy as np
import mayavi.mlab as mv
import os
os.environ['QT_API'] = 'pyqt'
os.environ['ETS_TOOLKIT'] = 'qt4'

class DrawCell():
    def __init__(self, cell):
        self.cell = cell
        self.start()
        self.draw_edges()
        self.draw_atoms()
        # self.draw_v_points()
        self.draw_voronoi()
        self.finish()

    def start(self):
        print('Drawing the cell...')
        self.fig = mv.figure('KKR.KIT')
        self.fig.scene.disable_render = True # Super duper trick

    def draw_edges(self):
        # origin = np.array([0,0,0])
        # latt_nodes = origin + self.cell.basis_a
        edges_dir = np.array([
            [ [0,0,0], [1,0,0], [0,1,0], [0,0,1] ],
            [ [1,1,1], [0,1,1], [1,0,1], [1,1,0] ],
            [ [1,0,0], [1,0,1], [1,1,0], [-1,-1,-1] ],
            [ [0,1,0], [0,1,1], [1,1,0], [-1,-1,-1] ],
            [ [0,0,1], [1,0,1], [0,1,1], [-1,-1,-1] ]
        ])
        edges_cart = np.dot(edges_dir, self.cell.basis_a)
        edge_color = (0,0,0)
        lw = 3
        for i in range(2):
            for j in range(3):
                mv.plot3d(
                    [ edges_cart[i,0,0], edges_cart[i,j+1,0] ],
                    [ edges_cart[i,0,1], edges_cart[i,j+1,1] ],
                    [ edges_cart[i,0,2], edges_cart[i,j+1,2] ],
                    color=edge_color, line_width=lw)
        for i in range(2,5):
            for j in range(2):
                mv.plot3d(
                    [ edges_cart[i,0,0], edges_cart[i,j+1,0] ],
                    [ edges_cart[i,0,1], edges_cart[i,j+1,1] ],
                    [ edges_cart[i,0,2], edges_cart[i,j+1,2] ],
                    color=edge_color, line_width=lw)

    def draw_atoms(self):
        for i, atom in enumerate(self.cell.atoms):
            print(f'Drawing atom {i}')
            c=0
            if atom.name == 'E':
                c = 0.5
            if atom.output_radius > 0:
                r = atom.output_radius
            else:
                r = atom.rmt_vor
            mv.points3d(*[*atom.pos_cart, r*2],
                    resolution=16, scale_factor=1,
                    color=(0,0+c,0.6), opacity=0.15)
            mv.text3d(*atom.pos_cart, atom.name, scale=0.4)

    def draw_v_points(self):
        if self.cell.num_E_to_find >= 0:
            v_points = self.cell.v_points
            mv.points3d(v_points[:,0], v_points[:,1], v_points[:,2],
                resolution=8, scale_factor=0.1, scale_mode='none',
                color=(0,1,0))

    def draw_voronoi(self):
        from kkr_tool import key_args
        for arg in key_args:
            if '--voronoi' in arg or '--vor' in arg:
                break
        else:
            return
        
        print('Drawing Voronoi polyhedra...')
        from scipy.spatial import ConvexHull
        vor_regions = self.cell.voronoi.regions
        vor_indicies = self.cell.voronoi.point_region[:self.cell.N_at]

        for i, atom in enumerate(self.cell.atoms):
            vertices = self.cell.voronoi.vertices[vor_regions[ self.cell.voronoi.point_region[i] ]]
            poly = ConvexHull(vertices)
            simplices = poly.simplices
            # s = poly.points[simplices]
            # for s in simplices:
            sx, sy, sz = vertices[:,0], vertices[:,1], vertices[:,2]
            mv.triangular_mesh(sx, sy, sz, poly.simplices,
            color=(1,0.0,0.0), opacity=0.2)

    def finish(self):
        mv.gcf().scene.parallel_projection = True
        self.fig.scene.disable_render = False # Super duper trick
        print('Cell drawn. Showing now...')
        mv.show()

# rmt_vasp = (rwigs_vasp) 

# cell.get_rmt_voronoi()
# rmt = cell.get_rmt_touching(True, rmt_vasp)
# v_points, _ = cell.get_v_points()
# # cell.add_empty(v_points[[5, 31, 13, 6, 32]] @ np.linalg.inv(cell.basis_a))



# cell.get_rws_from_rmt(True, rmt)


# np.set_printoptions(suppress=True)

# print(np.array2string(cell.at_pos_dir @ cell.basis, precision=16, floatmode='fixed'))
# print()
# print('SORTED:')
# ind = np.argsort((cell.at_pos_dir[:,-1]))
# print(ind)

# print(np.array2string(cell.at_pos_dir[ind] @ cell.basis, precision=14, floatmode='fixed'))
# # DRAW VORONOI
# if True:




    # at_colors = [(0.8,0,0), (0,0.8,0), (0,0,0.8),
    #          (0.8,0.8,0), (0.5, 0.5, 0),(0.5,0.5,0.5)]

    # typ_list = []
    # at_color_dict={}
    # for i, n in enumerate(cell.at_numbers):
    #     typ_list += [i]*n

    # if draw_v_points:
    #     # mv.points3d(v_points[:,0], v_points[:,1], v_points[:,2], 4*dist_field**3,
    #     #         resolution=8, scale_factor=0.1,
    #     #         color=(1,0,0))

    #     mv.points3d(v_points[:,0], v_points[:,1], v_points[:,2],
    #             resolution=8, scale_factor=0.1, scale_mode='none',
    #             color=(0,1,0))

        # for i, p in enumerate(v_points):
        #         mv.text3d(p[0], p[1], p[2], '  ' + str(i), scale=0.2)









