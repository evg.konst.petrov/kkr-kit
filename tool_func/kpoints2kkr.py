#!/usr/bin/env python3
import sys, os
import numpy as np
from kkr_tool import target

def get_rec_basis(target_str='file.str'):
    with open(target_str, 'r') as f:
        basis_lines = f.readlines()[4:7]

    for i in range(len(basis_lines)):
        basis_lines[i] = basis_lines[i].replace('=',' ')
    basis = np.loadtxt(basis_lines, usecols=(-5,-4,-3))
    rec_basis = np.linalg.inv(basis.T)

    return(rec_basis)

def get_vasp_kpoints(target):
    if target == '':
        target = 'KPOINTS'
    with open(target, 'r') as f:
        kpoints_file = f.readlines()
    kpoints = np.loadtxt(kpoints_file[4:], usecols=(0,1,2))
    ndiv = int(kpoints_file[1])
    knames = np.loadtxt(kpoints_file[4:], usecols=(-1), dtype=np.str)
    print(knames)
    return(ndiv, kpoints, knames)

def make_kkr_bandlines(kpoints, knames, ndiv, bandfile='bands.dat'):
    nbndr = len(kpoints)//2
    kkr_bandlines = [f'BANDS        nbndr={nbndr} nediv=2 band=1 bandfile={bandfile}']
    for i in range(0, nbndr):
        k1 = f'{kpoints[i*2, 0]:< 2.8f} {kpoints[i*2, 1]:< 2.8f} {kpoints[i*2, 2]:< 2.8f}'
        k2 = f'{kpoints[i*2+1, 0]:< 2.8f} {kpoints[i*2+1, 1]:< 2.8f} {kpoints[i*2+1, 2]:< 2.8f}'
        kkr_bandlines.append(f'{knames[i*2]}={k1}  {knames[i*2+1]}={k2}  ndiv={ndiv:<4}')
    return(kkr_bandlines)


def run(out_filename='input_bands'):
    ndiv, vasp_kpoints, knames = get_vasp_kpoints(target)
    rec_basis = get_rec_basis()
    kkr_kpoints = np.dot(vasp_kpoints, rec_basis)
    out = make_kkr_bandlines(kkr_kpoints, knames, ndiv)
    out = '\n'.join(map(str, out))
    print(out)
    with open(out_filename, 'w') as f:
        f.write(out)
