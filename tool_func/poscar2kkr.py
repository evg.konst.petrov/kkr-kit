#!/usr/bin/env python3
import numpy as np
from read_func.cell import Cell, Atom
from database.constants import *

class Poscar2kkr(Cell):
    def sort(self):
        # sort atoms along one of cartesian axes
        from kkr_tool import key_args
        for arg in key_args:
            if '--sort=' in arg:
                axis_str = arg.split('=')[1]
                print(f'Sorting along {axis_str.upper()} axis requested!')
                axis = {'x':0, 'y':1, 'z':2, 'X':0, 'Y':1, 'Z':2}[axis_str[0]]
                sort_indicies = np.argsort(self.at_pos_cart[:, axis])
                self.at_list = [self.at_list[ind] for ind in sort_indicies]
                self.at_pos_dir = self.at_pos_dir[sort_indicies]
                self.at_pos_cart = self.at_pos_cart[sort_indicies]

            elif '--sort-cluster=' in arg:
                # this will sort atoms along given axis, 
                # but also it will maintain minimum distance 
                # to center atom. center atom may be set manually.
                axis_str = arg.split('=')[1]
                print(f'Sorting and _clusterizing_ along {axis_str.upper()} axis requested!')
                axis = {'x':0, 'y':1, 'z':2, 'X':0, 'Y':1, 'Z':2}[axis_str[0]]

                # seaching center atom. this atoms will be remained as it is.
                if len(axis_str) > 1:
                    center_atom = int(axis_str[1:]) - 1
                else:
                    avg_coords = np.mean(self.at_pos[:, axis])
                    avg_deviation = np.abs(self.at_pos[:, axis] - avg_coords)
                    center_atom = np.argmin(avg_deviation)
                
                center_atom_coord = self.at_pos[center_atom]
                print(f'Center atom #{center_atom} has coord {center_atom_coord}')

                # making nodes for translations
                nodes_int = [[0, 0, 0]]
                for i in range(-1, 2):
                    for j in range(-1, 2):
                        for k in range(-1, 2):
                            if (i, j, k) != (0, 0, 0):
                                nodes_int.append([i, j, k])
                nodes_int = np.array(nodes_int)
                at_pos_dir = self.at_pos @ np.linalg.inv(self.basis)

                # calculating minimum deviations from center atom along given axis
                min_deviations = []
                for i, pos in enumerate(at_pos_dir):
                    nodes_pos = nodes_int + pos
                    nodes_pos_cart = nodes_pos @ self.basis
                    deviations = nodes_pos_cart[:,axis] - center_atom_coord[axis]
                    min_deviation = deviations[ np.argmin( np.abs(deviations) ) ]
                    min_deviations.append(min_deviation)

                # sorting
                sort_indicies = np.argsort(min_deviations)
                self.at_list = [self.at_list[ind] for ind in sort_indicies]
                self.at_pos = self.at_pos[sort_indicies]

    def make_header(self):
        max_len = 79
        header = f' Compound {self.name} '
        pad_len_l = ( max_len - len(header) ) // 2
        pad_len_r = max_len - len(header) - pad_len_l
        return('-'*pad_len_l + header + '-'*pad_len_r)

    def make_basis(self):
        basis_str = []
        vecs = 'abc'
        for v in range(3):
            basis_str.append( f'{self.basis[v, 0]:< 2.15f} {self.basis[v, 1]:< 2.15f} {self.basis[v, 2]:< 2.15f}  {vecs[v]}scale= 1.d0' )
        basis_str = '\n    '.join(map(str, basis_str))
        return(basis_str)

    def make_atomic_pos(self):
        tol = 15
        from kkr_tool import key_args
        for arg in key_args:
            if '--tautol=' in arg:
                tol = int(arg.split('=')[1])
        nat = -1.7
        sec = []
        for typ, pos, i in zip(self.at_list, self.at_pos_cart / self.latt_a, range(self.N_at)):
            pos_t = np.around(pos, decimals=tol) + 0.0
            sec.append( f'{i+1:2}. {typ:2} type={i+1:<2} nat={nat:3}  tau={pos_t[0]: < 2.{tol}f} {pos_t[1]: < 2.{tol}f} {pos_t[2]: < 2.{tol}f} sl=1   ')
        sec = '\n'.join(map(str, sec))
        return(sec)

    def make_atomic_conf(self):
        from database.at_configs import at_conf_dict
        at_types = list(np.sort(np.unique(self.at_list)))
        if 'E' in at_types:
            at_types.remove('E')
            at_types.append('E')


        sec = []
        for typ in at_types:
            nc, c, nv, v = at_conf_dict[typ][1:]
            conf_str = f'{self.at_list.index(typ)+1:2}. label={typ:<2}  nc={nc:<2}  c={c:<30}  nv={nv:1} v={v:<10} '
            sec.append(conf_str)
        sec = '\n'.join(map(str, sec))
        return(sec)

    def make_atomic_opt(self):
        sec = []
        for i, atom in enumerate(self.at_list):
            opt_str = f'{i+1:2}. atom={atom:<2}  type={i+1:<2}  fix=F  lmax=3  lmaxv=6  conc=1.0  mtz=T  sort={i+1}'
            sec.append(opt_str)
        sec = '\n'.join(map(str, sec))
        return(sec)

    def make_potentials(self):
        sec = []
        for i, atom in enumerate(self.at_list):
            pot_str = f'{i+1:2}. {atom:<2} type={i+1:<2}  np=1001  r1=1.0E-05  rnp=-1.000000  pfile={atom}_{i+1}.pot'
            sec.append(pot_str)
        sec = '\n'.join(map(str, sec))
        return(sec)

    def make_file_str(self):
        from read_func.func import load
        from kkr_tool import init_dir
        with open(f'{init_dir}/tool_templates/file.str.temp', 'r') as f:
            temp_file = f.read()
            temp_file = temp_file.\
            replace('!atomic positions!', self.make_atomic_pos()).\
            replace('!atomic configurations!', self.make_atomic_conf()).\
            replace('!atomic options!', self.make_atomic_opt()).\
            replace('!potentials!', self.make_potentials()).\
            replace('!alat!', str(round(self.latt_a*au, 15))).\
            replace('!basis!', self.make_basis()).\
            replace('!header!', self.make_header())
        return(temp_file)

    def __init__(self, filename):
        self.from_package = ''
        self.file = self.get_file(filename)
        self.latt_a = 0
        self.basis  = 0
        self.basis_a = 0
        self.cell_volume = 0
        self.N_at = 0
        self.at_pos_dir, self.at_pos_cart = [], []
        self.at_list = []
        self.atoms = []
        self.actual_init(self.from_package)
        self.atoms = [Atom(self, i) for i in range(self.N_at)]
        self.sort()

def run():
    from kkr_tool import target, key_args
    if target == '':
        target = 'POSCAR'
    file_str = Poscar2kkr(target).make_file_str()
    with open('file.str', 'w') as f:
        f.write(file_str)