#!/usr/bin/env python3
import sys
import os
import numpy as np
from kkr_tool import target


def load(filename):
    with open(filename, 'r') as f:
        file = f.readlines()
    return(file)


class kmesh2d():
    def load_ref(self, filename):
        '''
        ref-file sets the tetragon in k-space
        you want to mesh. This file may contain:
        0: dimensions of the mesh separated by space.
        1-4: coordinates of 4 corners of tetragon in k-space.
        5: (optional) input scaling: alat or direct (like in vasp). output scaling is always alat.
        The order is following:
        tl
        tr
        br
        bl

        example:
        200 200    
        -0.5 0.5 0 
        0.5 0.5 0  
        0.5 -0.5 0 
        -0.5 -0.5 0
        direct     
        '''
        return(load(filename))

    def get_ref_kpoints(self):
        return(np.loadtxt(self.ref_file[1:5], usecols=(0, 1, 2)))

    def check_scaling(self):
        scaling = 'alat'
        try:
            scaling = self.ref_file[5].split()[0]
        except:
            print('Can\'t find scaling for reference k-points coordinates. Assume they are scaled with alat (like Arthur does).')
            pass
        return(scaling)

    def check_complan(self):
        v1 = self.ref_kpoints[1] - self.ref_kpoints[0]
        v2 = self.ref_kpoints[2] - self.ref_kpoints[0]
        v3 = self.ref_kpoints[3] - self.ref_kpoints[0]
        v = np.linalg.det(np.vstack([v1, v2, v3]))
        if abs(v) >= 1e-6:
            print('ERROR!\nChosen reference k-points are NOT COPLANAR!\nYou were supposed to set a PLANE in k-space, not this piece of shit!')
            sys.exit()
        else:
            return(v)

    def get_mesh_dim(self):
        dim = np.loadtxt([self.ref_file[0]], usecols=(0, 1), dtype=np.int)
        print(f'2D k-mesh dimensions: {dim}')
        return(dim)

    def get_rec_basis_a(self):
        from read_func.str import Structure
        struct = Structure()
        rec_basis_a = struct.rec_basis_a
        return(rec_basis_a)

    def get_rec_basis_alat(self):
        from read_func.str import Structure
        struct = Structure()
        rec_basis_alat = struct.rec_basis_alat
        return(rec_basis_alat)

    def get_rec_basis(self):
        from read_func.str import Structure
        struct = Structure()
        rec_basis = struct.rec_basis
        return(rec_basis)

    def gen_mesh(self):
        dkx = (self.ref_kpoints[1] - self.ref_kpoints[0])
        dky = (self.ref_kpoints[3] - self.ref_kpoints[0])
        ckx = np.linspace(0, 1, self.dim[0])
        cky = np.linspace(0, 1, self.dim[1])
        kmesh = [self.ref_kpoints[0] + dkx*cx + dky*cy
                 for cx in ckx for cy in cky]
        kmesh = np.reshape(kmesh, (np.prod(self.dim), 3))

        # scaling with alat
        if self.scaling == 'alat':
            pass
        elif self.scaling == 'direct':
            # g_matrix = np.loadtxt(self.ref_file[6:9])
            kmesh = np.dot(kmesh, self.rec_basis_alat)
        return(kmesh)

    def print_basis(self):
        text = ['Reciprocal basis (direct)', 'Reciprocal basis (alat scaled, Arthur style)']
        for b, basis in enumerate([self.get_rec_basis(), self.get_rec_basis_alat()]):
            print(text[b])
            print(basis)
            print()

    def save_kmesh(self):
        name = f'kmesh.{self.dim[0]}.{self.dim[0]}'
        ones = np.ones(np.prod(self.dim))
        mesh_w = np.concatenate((self.kmesh, ones[:, None]), axis=1)
        np.savetxt(name, mesh_w, delimiter=' ', fmt='%12.9f',
                   header='scale=alat', comments='')


    def __init__(self):
        self.ref_file = self.load_ref(target)
        self.dim = self.get_mesh_dim()
        self.ref_kpoints = self.get_ref_kpoints()
        self.scaling = self.check_scaling()
        self.vol = self.check_complan()
        self.print_basis()
        self.rec_basis_a = self.get_rec_basis_a()
        self.rec_basis_alat = self.get_rec_basis_alat()
        self.kmesh = self.gen_mesh()


def run():
    kmesh = kmesh2d().save_kmesh()
