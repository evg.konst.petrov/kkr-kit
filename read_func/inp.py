#!/usr/bin/env python3
import numpy as np
import os
from read_func.func import *
from kkr_read import key_args

ryd = 13.6056980659
# class for reading input-file


class Inp:
    '''
    A class for dealing with KKR input-file
    '''

    def get_val(self, param):
        '''
        function to get certain parameter value 
        from list (file).
        returns string!
        '''
        lines = find_lines(param, self.file)
        if len(lines) > 1:      # if we have several matches, some paramter has close name (like etype and qetype)
            for line in lines:
                param_s = ' ' + param
                if param in lines[0]:
                    val = lines[0].split(param_s + '=')[1].split()[0]
                    return(val)
                    break
        elif len(lines) == 1:
            if param in lines[0]:
                val = lines[0].split(param+'=')[1].split()[0]
                return(val)
        elif len(lines) == 0:
            print(f'Parameter {param} is not in input-file.')
            return('')

    def get_val_all(self, param):
        '''
        function to get certain parameter value 
        from list (file).
        returns string!
        '''
        vals = []
        lines = find_lines(param, self.file)
        if len(lines) >= 1:
            for line in lines:
                vals.append(str2val(param, line))
            return(vals)
        elif len(lines) == 0:
            print(f'Parameter {param} is not in file.')
            return('')

    def get_bands_spec_k(self):
        '''
        This function returns list of knames and 
        list of special BZ indicies along kpath.
        '''
        i = int(find_index_1('nbndr', self.file))
        nbndr_str = str2val('nbndr', self.file[i])
        try:
            nbndr = int(nbndr_str)
            bands_lines = self.file[i+1:i+nbndr+1]
            for i, line in enumerate(bands_lines):
                bands_lines[i] = bands_lines[i].replace('=', ' ')
            knames_all = np.loadtxt(bands_lines,
                                    dtype='str', usecols=(0, 4)).flatten()
            knames = [knames_all[0]]
            kindex = [0]
            ndivs = np.loadtxt(bands_lines, dtype=np.int, usecols=(-1))
            kindex_all = sorted(list(np.cumsum(ndivs)) +
                                list(np.cumsum(ndivs)[:-1]-1))
            kindex_all[-1] += -1
            for i, name in enumerate(knames_all[1:]):
                if name != knames[-1]:
                    knames.append(name)
                    kindex.append(kindex_all[i])
            self.knames = knames
            self.kindex = list(kindex)
        except:
            print('WARNING! \nIt seems that \'nbndr\' parameter is not integer, but string. This means that you used separate external k-points file, so \'knames\' and \'kindex\' can not be determined automatically. Sorry!\n')
            knames = []
            kindex = []
            self.ext_kpoints = True
            self.ext_kpoints_filename = nbndr_str
        return(knames, kindex)

    def get_nspin(self):
        nspin = 0
        keyv = self.get_val('keyv')
        relnew = self.get_val('relnew')
        if keyv == '0' and relnew == '1':
            nspin = 3
        elif keyv in ['2', '4']:
            nspin = 2
        elif keyv in ['1', '3', '5']:
            nspin = 1
        return(nspin)

    def get_nbndr(self):
        nbndr_str = self.get_val('nbndr')
        try:
            nbndr = int(nbndr_str)
        except:
            nbndr = nbndr_str
            if '-2d' not in key_args:
                print('\'nbndr\'- parameter is not an INT, but 2d mode is NOT ON. \nIt is very likely that this will not going to be productive.')

    def __init__(self, filename='input'):
        self.file = load(filename)
        self.name = self.get_val('COMPOUND')
        self.job = self.get_val('job')
        self.nep = int(self.get_val('nep'))
        self.efermi = float(self.get_val('efermi'))*ryd
        self.emin = float(self.get_val('emin'))*ryd
        self.emax = float(self.get_val('emax'))*ryd
        self.str_filename = self.get_val('strfile')
        self.nspin = self.get_nspin()
        self.nbndr = self.get_nbndr()
