#!/usr/bin/env python3
import numpy as np
import os

# some common functions to parse files
def load(filename):
    with open(filename, 'r') as f:
        file = f.readlines()
        return(file)

# def create_dir():
#     '''
#     this function creates directory for data.
#     if current directory is being processed, it will create 
#     dir _data_NAME_JOB. 
#     if processing directory was specified, it will create dir
#     _data_NAME_CALCNAME in common directory _data_NAME 
#     (CALCNAME = name of directory being processed)
#     '''
#     from kkr_read import inp, d, dirname, if_curr_dir, out_dir
#     if if_curr_dir:
#         dirname = f'_data_{inp.name}_job={inp.job}'
#         # plot_name = f'{inp.name}_job={inp.job}'
#     else:
#         dirname = f"_data_{inp.name}_{dd}"
#     try:
#         os.mkdir(out_dir + dirname)
#         print(f'Creating directory {dirname}...')
#     except:
#         pass
#     os.chdir(out_dir + dirname)
#     return(dirname)

def find_line_1(string, file):
    for line in file:
        if string in line:
            break
    return(line)

def find_lines(string, file):
    lines = []
    for line in file:
        if string in line:
            lines.append(line)
    return(lines)

def get_val(param, file):
    '''
    function to get certain parameter value 
    from list (file).
    returns string!
    '''
    lines = find_lines(param, file)

    # if we have several matches, some paramter has close name (like etype and qetype)
    if len(lines) > 1:      
        for line in lines:
            param_s = ' ' + param
            if param in line:
                val = line.split(param_s + '=')[1].split()[0]
                break
        return(val)
                
    elif len(lines) == 1:
        if param in lines[0]:
            val = lines[0].split(param+'=')[1].split()[0]
            return(val)
    elif len(lines) == 0:
        print(f'Parameter {param} is not in file.')
        return('')

def get_all_vals(param, file):
    '''
    function to get certain parameter value 
    from list (file).
    returns string!
    '''
    vals = []
    lines = find_lines(param, file)
    if len(lines) >= 1:
        for line in lines:
            vals.append(str2val(param, line))
        return(vals)
    elif len(lines) == 0:
        print(f'Parameter {param} is not in file.')
        return('')

def str2val(param, string):
    '''
    function to get certain parameter value 
    from string.
    returns string!
    '''
    if param in string:
        val = string.split(param+'=')[1].split()[0]
        return(val)
    else:
        print(f'Error in str2val: parameter \'{param}\' is not in this string.')

def find_index_1(string, file):
    for i, line in enumerate(file):
        if string in line:
            break
    return(i)


def save_templates(templates_dict):
    from kkr_read import kit_dir
    for template in templates_dict:
        os.system(f'cp {kit_dir}/fig_templates/{template} ./{templates_dict[template]} ')

def write_info(info_dict):
    with open('info.py', 'w') as w:
        w.write('#!/usr/bin/env python3\n\n')
        for info in info_dict:
            tmp = info_dict[info]
            if type(tmp) == str:
                tmp = f"\'{tmp}\'"
            else:
                pass
            w.write(f'{info} = {tmp} \n')