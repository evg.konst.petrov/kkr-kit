#!/usr/bin/env python3
import numpy as np
import os
from read_func.func import *


class Structure:
    '''
    strcucture-file reading class
    '''
    @property
    def basis(self):
        basis_lines = \
            [line.replace('=', ' ') for line in self.file[4:7]]
        basis = np.loadtxt(basis_lines, usecols=(-5, -4, -3))
        return(basis)

    @property
    def latt_param(self):
        params = get_val('alat', self.file), \
            get_val('blat', self.file), \
            get_val('clat', self.file)
        return(np.array(params, dtype=np.float))

    @property
    def basis_a(self):
        basis_a = self.basis*self.latt_param
        return(basis_a)

    @property
    def rec_basis(self):
        rec_basis = np.linalg.inv(self.basis.T)
        return(rec_basis)

    @property
    def rec_basis_a(self):
        rec_basis_a = np.linalg.inv(self.basis_a.T)*np.pi*2
        return(rec_basis_a)

    @property
    def rec_basis_alat(self):
        rec_basis_alat = self.rec_basis_a/np.linalg.norm(self.rec_basis_a[0])
        return(rec_basis_alat)

    @property
    def dat_names(self):
        '''
        this .individual .dat-names can be handy 
        if you want to get site-resolved DOS or BSF.
        '''
        names = []
        for name in self.pot_names:
            if '.pot' in name:
                names.append(name.replace('.pot', '.dat'))
            else:
                names.append(name.replace('.pot', ''))
        return(names)

    @property
    def atom_names(self):
        k = find_index_1('- potentials -', self.file) + 2
        at_names = []
        for line in self.file[k:]:
            if '----' in line:
                break
            else:
                at_names.append(line.split()[1])
        return(at_names)

    def check_type_order(self):
        # collect atomic types
        i = find_index_1('- atomic -', self.file) + 2
        at_types = []
        for line in self.file[i:]:
            if '----' in line:
                break
            else:
                at_types.append(int(str2val('type', line)))
        # collect potential types
        j = find_index_1('- potentials -', self.file) + 2
        pot_types = []
        for line in self.file[j:]:
            if '----' in line:
                break
            else:
                at_types.append(int(str2val('type', line)))
        # check if they are different
        if at_types != pot_types:
            print('Your structure-file is messed up! The ordering of ATOMIC OPTIONS and POTENTIALS sections is different.\nPROCEED WITH CAITION!')

    @property
    def concentrations(self):
        conc = get_all_vals('conc', self.file)
        conc = [float(c) for c in conc]
        return(conc)

    # TODO
    @property
    def site_names(self):
        pass

    def __init__(self, filename='file.str'):
        self.filename = filename
        self.file = load(self.filename)
        self.pot_names = get_all_vals('pfile', self.file)
        self.name = self.file[1].split()[2]
