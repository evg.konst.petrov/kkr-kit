#!/usr/bin/env python3
import numpy as np
import os
from read_func.func import *
from kkr_read import inp
from kkr_read import structure

class Dos:
    '''
    a class to deal with DOS
    '''
    def get_nep(self):
        # Suggested by Martin
        # check before reading dos, if acon used (nep will be changed according to 'esfile')
        acon = inp.get_val('acon')
        if acon != '0':
        # read esfile, if existing: first line contains int values
        #   (second one for number of energy points)
            esfile = load('esfile')
            nep = int( nep = esfile[0].split()[1] )
            print(f'WARNING! acon (analytic continuation) was used. Changed nep to value in esfile: {nep}')
        else:
            nep = inp.nep
        return(nep)

    @property
    def egrid(self):
        if self.relnew == '1':
            egrid = np.loadtxt(self.dos_atom_files[0][:self.nep], usecols=(1))
        else:
            egrid = np.loadtxt(self.dos_tot_file[:self.nep], usecols=(1))
        egrid *= ryd
        print(f'Got energy grid of {self.nep} points from {np.min(egrid)} to {np.max(egrid)} eV.')
        return(egrid)

    @property
    def dos_tot(self):
        '''
        Collect total DOS from dos.dat
        '''
        if self.relnew == '1':
            print('WARNING! \nTotal DOS can\'t be read with relnew=1! Total DOS will be constructed using individual atomic DOS.')
            DOS_tot = np.sum(self.dos_atom, axis=(0,2))[0]
        elif self.relnew != '1':
            # number of columns in DOS file
            ncol = len( self.dos_tot_file[0].split() )
            # number of components:
            # 1 = total 
            # 3 = total, s+, s-
            # 4 = total, s_x, s_y, s_z, but this 
            # is not gonna happen here
            ncomp = len( range(4, ncol, 2) )
            DOS_tot = np.reshape( \
                      np.loadtxt(self.dos_tot_file, 
                      usecols=range(4, ncol, 2)),
                      (self.nep, ncomp) )
            DOS_tot = np.swapaxes(DOS_tot, 0, 1)
        return(DOS_tot)

    #TODO
    @property
    def dos_atom(self):
        dat_names = structure.dat_names
        conc = structure.concentrations
        N_types = len(dat_names)
        # need to check if number of lm-components may be different
        N_lms = 16
        # get number of spin components
        if inp.nspin == 3:
            nspincomp = 4
        elif inp.nspin == 2:
            nspincomp = 3
        elif inp.nspin == 1:
            nspincomp = 1
        # print(nspincomp)

        # create zeros array for all those DOSes
        dos_atom = np.zeros((N_types, nspincomp, N_lms+1, self.nep),
                   dtype=np.float16)
        for typ, file in enumerate(self.dos_atom_files):
            for e in range(self.nep):
                # do DOS line by line
                dos_line = np.array(file[e].split()[2:], dtype=np.float16)
                # print(dos_line)
                # CONCENTRATION CONSIDERED HERE
                dos_line *= conc[typ]
                # first set total DOS for each spin
                dos_atom[typ, :, 0, e] = \
                    dos_line[range(nspincomp)]
                # now interate ovel other components
                if nspincomp != 1:
                    for s in range(1, nspincomp):
                        start = nspincomp + N_lms*(s-1)
                        end = start + N_lms 
                        # print(s, start, end, len(dos_line))
                        dos_atom[typ, s, 1:, e] = \
                            dos_line[start:end]
                elif nspincomp == 1:
                    start = 1
                    end = N_lms + 1
                    dos_atom[typ, 0, 1:, e] = \
                            dos_line[start:end]
            # apparently we have some some free space 
            # in this array, so we add spinless lm-resolved DOS
            if nspincomp != 1:
                dos_atom[typ, 0, :, :] = np.sum(
                    dos_atom[typ,:,:,:], axis=(0) )
        return(dos_atom)




    @property
    def dos_help(self):
        if self.nspin == 1:
            dos_help = '''This is _non_-spinpolarized calculation! 
            Total DOS has only one component.

            Atomic DOS array has following indicies: [atom, spin (1), lm, enegry point]'''.replace('\t', '\n')
        elif self.nspin == 2:
            dos_help = '''This is _spinpolarized_ calculation!
            Total DOS has 3 spin components: Total DOS, DOS_up, DOS_down
            
            Atomic DOS array has following indicies: [atom, spin (3), lm, enegry point]'''.replace('\t', '\n')
        elif self.nspin == 3:
            dos_help = '''This is _non-collinear_ calculation!
            Total DOS has 4 spin components: Total DOS, DOS_x, DOS_y, DOS_z
            
            Atomic DOS array has following indicies: [atom, spin (4), lm, enegry point]'''.replace('\t', '\n')
        elif self.nspin == 0:
            print('\nSOMETHING IS WRONG! \nnspin=0! WTF?\n')
        return(dos_help)

    def __init__(self, filename = 'dos.dat'):
        from kkr_read import inp
# number of energy points
# check before reading dos, if acon used (nep will be changed according to 'esfile')
        acon = inp.get_val('acon')
        if acon != '0':
        # read esfile, if existing: first line contains int values
        #   (second one for number of energy points)
            esfile = load('esfile')
            nep = esfile[0].split()[1]
            self.nep = int(nep)
            print('WARNING! acon (analytic continuation) was used. Changed nep to value in esfile: '+nep)
        else:
            self.nep = inp.nep
        
        self.efermi = inp.efermi
        self.filename = filename
        self.dos_tot_file = load(self.filename)[1:]
        self.dos_atom_files = \
            [ load(f)[1:] for f in structure.dat_names ]
        self.emin = inp.emin
        self.emax = inp.emax
        self.keyv = inp.get_val('keyv')
        self.relnew = inp.get_val('relnew')
        self.nspin = inp.nspin
        
        # output filenames
        self.out_names = {
            'dos_tot':'data_DOS',
            'dos_atom':'data_DOS_atom',
            'egrid':'data_egrid'
        }
    


