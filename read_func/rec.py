#!/usr/bin/env python3
import numpy as np
import sys
# from read_func.func import *

'''
a set of functions to deal with reciprocal space.
'''


def get_kpoints(obj):
    if '.Bloch.' in str(type(obj)):
        kpoints = np.loadtxt(obj.file, usecols=(3, 4, 5),
                             dtype=np.float32)[::obj.nep]
    else:
        print('!!! obj type in not bloch, dont know what to do!!! ')
    return(kpoints)


def get_kpath(obj):
    if '.bloch.' in str(type(obj)):
        kpath = np.loadtxt(obj.file, usecols=(2), dtype=np.float32)[::obj.nep]
    elif '.magnon.' in str(type(obj)):
        kpath = np.loadtxt(obj.band_filename,
                           usecols=(0), dtype=np.float32)
    else:
        print('!!! obj type in not bloch, dont know what to do!!! ')
    return(kpath)


def get_kpath_2d(obj):
    """
    In the case of 2d k-grid (line for Fermi surface slice)
    the dimensions of arectangular k-plane would be 
    calculated.
    """
    if '.Bloch.' in str(type(obj)):
        Nk = len(obj.kpoints)

        dk1 = obj.kpoints[1] - obj.kpoints[0]
        for k in range(1, Nk):
            dk2 = obj.kpoints[k] - obj.kpoints[0]
            if abs(np.inner(dk1, dk2)) < 1e-6:
                break

        nkx = k
        nky = Nk // nkx

        if nkx*nky == Nk:
            pass
        else:
            print("bloch.get_kpath_2d() complains: nkx*nkx != Nk. Sorry.")
            sys.exit()

        k_corners = obj.kpoints[[0, nky-1, Nk-1-nky, -1]]
        Lkx = np.linalg.norm(k_corners[1] - k_corners[0])
        Lky = np.linalg.norm(k_corners[3] - k_corners[0])
        kpath = [np.linspace(0, Lkx, nkx), np.linspace(0, Lky, nky)]
        return(kpath)
