#!/usr/bin/env python3
import numpy as np
import os
from read_func.func import *
import sys

# class for reading magnons
class Magnon:
    @property
    def kpoints(self):
        kpoints = np.loadtxt(self.magnon_file, usecols=(1,2,3))
        return(kpoints)

    @property
    def kpath(self):
        return( np.loadtxt(self.magnon_file, usecols=(0)) )

    @property
    def magnon(self):
        n_col = len(self.jq_file[0].split()) - 4
        # second set of columns has to be in meV
        columns_to_read = range(4 + n_col//2, 4 + n_col)
        magnon = np.loadtxt(self.magnon_file, usecols=columns_to_read)
        self.n_mag_sites = n_col // 2
        print(f'{self.n_mag_sites} magnetic sites detected!')
        return(magnon)
        
    @property
    def jq(self):
        n_col = len(self.jq_file[0].split()) - 4
        columns_to_read = range(4, 4 + n_col)
        # are they in mRy?
        jq = np.loadtxt(self.jq_file, usecols=columns_to_read)*ryd
        return(jq)

    #TODO
    @property
    def jrs(self):
        j0rs_file = load('j0rs.dat')
        sites_1, sites_2 = [], []
        for line in j0rs_file[1:]:
            sites_1.append(int(line.split()[0]))
            sites_2.append(int(line.split()[2]))



    def __init__(self):
        from kkr_read import inp
        self.n_mag_sites = 0
        self.magnon_filename = inp.get_val('bandfile')
        self.magnon_file = load(self.magnon_filename)
        self.jrs_filename = 'tmp_jrs.dat'
        self.jrs_file = load(self.jrs_filename)
        self.jq_filename = 'jq.dat'
        self.jq_file = load(self.jq_filename)
        self.knames, self.kindex = inp.get_bands_spec_k()
        
        # output filenames
        self.out_names = {
            'kpath':'data_kpath',
            'kpoints':'data_kpoints',
            'magnon':'data_magnon',
            'jq': 'data_jq',
            'jrs': 'data_jrs'
        }
