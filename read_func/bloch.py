#!/usr/bin/env python3
import numpy as np
import os
from read_func.func import *
from kkr_read import inp, key_args
import sys
from database.constants import *

# class for reading bloch spectral function


class Bloch:
    '''
    a class to deal with BSF
    '''

    def load_bsf_file(self, bsf_filename):
        bsf_file = load(bsf_filename)
        ncol = len(bsf_file[0].split())
        ncol_help = int(self.bsf_help.split('\n')[-2].split()[0])
        self.bsf_ncol = ncol_help - 6
        
        if ncol < ncol_help:
            print(
                f'Broken BSF lines detected! There sould be {ncol_help} columns in BSF file, but i got {ncol} instead. \nTrying to hoodwink it...')
            nl = ncol_help // ncol + 1
            n_prog = 60
            lines_per_k = ncol_help // ncol + 1
            Nk = len(bsf_file) // lines_per_k
            for k in range(Nk):
                #                print(bsf_file[k*(self.nep + 1) ].split()[0])
                # fancy progress bar
                prog = int((k+1)/Nk*n_prog)
                print('[' + '|'*int(prog) + ' '*int((n_prog-prog)) + ']', end='\r')

                for e in range(self.nep):
                    bsf_file[k*(self.nep + 1) + e: k*(self.nep + 1) + e + nl] = \
                        [' '.join(bsf_file[k*(self.nep+1) +
                                           e:k*(self.nep+1) + e + nl])]
            print(
                f'\nI think i fixed it. Now there should be { len( bsf_file[0].split()) } colums in BSF file.')
        return(bsf_file)

    def get_kpoints(self):
        kpoints = np.loadtxt(self.bsf_file[::self.nep+1],
                             usecols=(3, 4, 5), dtype=np.float32)
        return(kpoints)

    def get_egrid(self):
        try:
            egrid = np.loadtxt(self.bsf_file[:self.nep], usecols=(0))*ryd
            print(
                f'Got energy grid of {self.nep} points from {np.min(egrid)} to {np.max(egrid)} eV.')
        except:
            print(
                'WARNING! \nFailed to load egrid from BSF file.\nIt will be calculated with np.linspace.\n')
            egrid = np.linspace(self.emin, self.emax, self.nep)
        return(egrid)

    def get_bsf_help(self):
        bsf_help = load('bsf.help')
        return(''.join(map(str, bsf_help)))

    @property
    def kpath(self):
        kpath = np.loadtxt(self.bsf_file[::self.nep+1],
                           usecols=(2), dtype=np.float32)
        return(kpath)

    @property
    def kpath_2d(self):
        Nk = len(self.kpoints)
        dk1 = self.kpoints[1] - self.kpoints[0]
        for k in range(1, Nk):
            dk2 = self.kpoints[k] - self.kpoints[0]
            if abs(np.inner(dk1, dk2)) < 1e-6:
                break
        nkx = k
        nky = Nk // nkx

        if nkx*nky == Nk:
            pass
        else:
            print("bloch.get_kpath_2d() complains: nkx*nkx != Nk. Sorry.")
            sys.exit()
        k_corners = self.kpoints[[0, nky-1, Nk-1-nky, -1]]
        Lkx = np.linalg.norm(k_corners[1] - k_corners[0])
        Lky = np.linalg.norm(k_corners[3] - k_corners[0])
        kpath = [np.linspace(0, Lkx, nkx), np.linspace(0, Lky, nky)]
        return(kpath)

    @property
    def bsf(self):
        print('Reading BSF values ...')
        BSF = np.reshape(np.loadtxt(self.bsf_file,
                                    usecols=np.arange(6, self.bsf_ncol+6), dtype=np.float32),
                         (self.Nk, self.nep, self.bsf_ncol)).T
        return(BSF)

    @property
    def bsf_2d(self):
        print('Reading BSF values on 2D k-grid...')
        BSF = np.loadtxt(self.bsf_file, usecols=np.arange(
            6, self.bsf_ncol+6), dtype=np.float32).T
        return(BSF)

    def get_bsf_site_res(self):
        pass

    def __init__(self):
        # from kkr_read import inp, key_args
        import read_func.rec as rec
        self.nep = inp.nep
        self.efermi = inp.efermi
        self.ibsf = inp.get_val('ibsf')
        self.emin = inp.emin
        self.emax = inp.emax
        self.knames, self.kindex = inp.get_bands_spec_k()
        self.nspin = inp.nspin

        self.bsf_ncol = 0
        self.bsf_help = self.get_bsf_help()
        self.bsf_filename = inp.get_val('bandfile')
        self.bsf_file = self.load_bsf_file(self.bsf_filename)

        self.egrid = self.get_egrid()
        self.kpoints = self.get_kpoints()
        self.Nk = len(self.kpoints)

        self.out_names = {
            'bsf': 'data_BSF',
            'egrid': 'data_egrid',
            'kpoints': 'data_kpoints',
            'kpath': 'data_kpath'
        }
