#!/usr/bin/env python3
import numpy as np
from read_func.func import *
from database.constants import *
import sys

class Atom():
    def __init__(self, cell, i):
        self.pos_dir = cell.at_pos_dir[i]
        self.pos_cart = cell.at_pos_cart[i]
        self.pos_dir_trans = []
        self.pos_cart_trans = []
        self.name = cell.at_list[i]
        self.number = i
        self.rmt = 0.0
        self.rmt_vor = 0.0
        self.rws = 0.0
        self.rws_vor = 0.0
        self.vor_vol = 0.0
        self.output_radius = -1.0/au
        self.coo_radii = []
        self.coo_atoms_ind_trans = []
        self.coo_atoms_ind = []
        self.coo_overlaps = []

    @property
    def rmt_au(self):
        return(self.rmt * au )

    @property
    def rws_au(self):
        return(self.rws * au )

    @property
    def rmt_vor_au(self):
        return(self.rmt * au )

class Cell():
    ''' Unit cell class.
        All lengts units are ANGSTOMS! '''
    def __init__(self, filename):
        self.from_package = ''
        self.file = self.get_file(filename)
        self.latt_a = 0
        self.basis  = 0
        self.basis_a = 0
        self.cell_volume = 0
        self.N_at = 0
        self.at_pos_dir, self.at_pos_cart = [], []
        self.at_list = []
        self.atoms = []
        self.actual_init(self.from_package)
        self.atoms = [Atom(self, i) for i in range(self.N_at)]
        self.sort()

    def get_file(self, filename):
        with open(filename, 'r') as f:
            file = f.readlines()
        for i, line in enumerate(file):
            file[i] = line.replace('\t', '  ')
        
        condition_HUTSEPOT = '---' in file[0] and '---' in file[2] and 'alat' in file[3] and 'rb=' in file[4]
        condition_VASP = len(file[1].split()) == 1 and len(file[2].split()) == 3 and len(file[3].split()) == 3 and len(file[4].split()) == 3
        
        if condition_HUTSEPOT and not condition_VASP:
            self.from_package = 'HUTSEPOT'
            print('Trying te get cell from HUTSEPOT structure file...')
        elif not condition_HUTSEPOT and condition_VASP:
            self.from_package = 'VASP'
            print('Trying te get cell from VASP structure file...')
        else:
            print('ERROR!')
            print('Unknown type of structure file.')
            sys.exit()

        if self.from_package in ['VASP']:
            if any(s in file[7] for s in ['Cart', 'cart', 'Dir', 'dir']):
                file.insert(7, '  ')
        elif self.from_package in ['KKR', 'HUTSEPOT']:
            pass
        return(file)

    def get_at_list_VASP(self):
        at_names = self.file[5].split()
        at_numbers = np.array(self.file[6].split(), dtype=np.int)
        at_list = []
        for number, typ in zip(at_numbers, at_names):
            at_list += [typ]*number
        return(at_list)

    def get_at_list_HUTSEPOT(self):
        at_list = []
        for line in self.file[8:]:
            if 'type=' in line:
                at_list.append( line.split()[1] )
            elif '---' in line and len(at_list) != 0:
                break
        return(at_list)

    def get_pos_VASP(self):
        start = 7
        for i, line in enumerate(self.file[start:]):
            if 'Direct' in line or 'direct' in line:
                cart = False
                break
            elif 'Cart' in line or 'cart' in line:
                cart = True
                break
        start = start + i + 1
        at_pos_dir = np.loadtxt(self.file[start:start + self.N_at], usecols=(0,1,2))
        at_pos_dir = np.mod(at_pos_dir, 1)
        if not cart:
            at_pos_cart = at_pos_dir @ self.basis_a
        else:
            pass
        return(at_pos_dir, at_pos_cart)

    def get_pos_HUTSEPOT(self):
        at_pos_cart_au = []
        for line in self.file[8:]:
            if 'tau=' in line:
                tmp = line.split('tau=')[-1].split()[0:3]
                at_pos_cart_au.append(tmp)
            elif '---' in line and len(at_pos_cart_au) != 0:
                break
        at_pos_cart = np.array(at_pos_cart_au, dtype=float) * self.latt_a
        at_pos_dir = at_pos_cart @ np.linalg.inv(self.basis_a)
        return(at_pos_dir, at_pos_cart)

    def actual_init(self, from_package):
        if from_package == 'VASP':
            self.name = self.file[0].strip().replace(' ', '_')
            # set lattice constant
            self.latt_a = float(self.file[1])
            # set basis (scaled and not scaled)
            self.basis  = np.loadtxt(self.file[2:5])
            self.basis_a = self.basis*self.latt_a
            # set cell volume
            self.cell_volume = np.linalg.det(self.basis_a)
            # get list of atoms and total number of atoms
            self.at_list = self.get_at_list_VASP()
            self.N_at = len(self.at_list)
            # get atomic positions
            self.at_pos_dir, self.at_pos_cart = self.get_pos_VASP()
            self.pot_names = None
            
        elif from_package == 'HUTSEPOT':
            self.name = self.file[1].split()[2].replace(' ', '_')
            # set lattice constant
            self.latt_a = np.array(
                        [ float(get_val('alat', self.file)),
                          float(get_val('blat', self.file)),
                          float(get_val('clat', self.file)) ] ) / au
            basis_lines = self.file[4:7]
            basis_lines[0] = basis_lines[0].replace('rb=', '   ')
            # set basis (scaled and not scaled)
            self.basis = np.loadtxt(basis_lines, usecols=(0,1,2))
            self.basis_a = self.basis*self.latt_a

            # set cell volume
            self.cell_volume = np.linalg.det(self.basis_a)
            # get list of atoms and total number of atoms
            self.at_list = self.get_at_list_HUTSEPOT()
            self.N_at = len(self.at_list)
            self.pot_names = get_all_vals('pfile', self.file)
            # get atomic positions
            self.at_pos_dir, self.at_pos_cart = self.get_pos_HUTSEPOT()
            # make all alat scaled, without blat and clat
            self.latt_a = self.latt_a[0]
            self.basis = self.basis_a / self.latt_a

    def sort(self):
        # sort atoms along one of cartesian axes
        try:
            from kkr_tool import key_args
        except:
            print('Can\'t import sorting args from kkr_tool.')
            return

        for arg in key_args:
            if '--sort=' in arg:
                axis_str = arg.split('=')[1]
                print(f'Sorting along {axis_str.upper()} axis requested!')
                axis = {'x':0, 'y':1, 'z':2, 'X':0, 'Y':1, 'Z':2}[axis_str[0]]
                sort_indicies = np.argsort(self.at_pos_cart[:, axis])
                self.at_list = [self.at_list[ind] for ind in sort_indicies]
                self.at_pos_dir = self.at_pos_dir[sort_indicies]
                self.at_pos_cart = self.at_pos_cart[sort_indicies]
                atoms_new = []
                for i in sort_indicies:
                    atoms_new.append(self.atoms[i])
                self.atoms = atoms_new[:]
                del(atoms_new)

            # elif '--sort-cluster=' in arg:
            #     # this will sort atoms along given axis, 
            #     # but also it will maintain minimum distance 
            #     # to center atom. center atom may be set manually.
            #     axis_str = arg.split('=')[1]
            #     print(f'Sorting and _clusterizing_ along {axis_str.upper()} axis requested!')
            #     axis = {'x':0, 'y':1, 'z':2, 'X':0, 'Y':1, 'Z':2}[axis_str[0]]

            #     # seaching center atom. this atoms will be remained as it is.
            #     if len(axis_str) > 1:
            #         center_atom = int(axis_str[1:]) - 1
            #     else:
            #         avg_coords = np.mean(self.at_pos[:, axis])
            #         avg_deviation = np.abs(self.at_pos[:, axis] - avg_coords)
            #         center_atom = np.argmin(avg_deviation)
                
            #     center_atom_coord = self.at_pos[center_atom]
            #     print(f'Center atom #{center_atom} has coord {center_atom_coord}')

            #     # making nodes for translations
            #     nodes_int = [[0, 0, 0]]
            #     for i in range(-1, 2):
            #         for j in range(-1, 2):
            #             for k in range(-1, 2):
            #                 if (i, j, k) != (0, 0, 0):
            #                     nodes_int.append([i, j, k])
            #     nodes_int = np.array(nodes_int)
            #     at_pos_dir = self.at_pos @ np.linalg.inv(self.basis)

            #     # calculating minimum deviations from center atom along given axis
            #     min_deviations = []
            #     for i, pos in enumerate(at_pos_dir):
            #         nodes_pos = nodes_int + pos
            #         nodes_pos_cart = nodes_pos @ self.basis
            #         deviations = nodes_pos_cart[:,axis] - center_atom_coord[axis]
            #         min_deviation = deviations[ np.argmin( np.abs(deviations) ) ]
            #         min_deviations.append(min_deviation)

            #     # sorting
            #     sort_indicies = np.argsort(min_deviations)
            #     self.at_list = [self.at_list[ind] for ind in sort_indicies]
            #     self.at_pos = self.at_pos[sort_indicies]

    def save(self):
        print('Constructting file.str...')
        # make header
        max_len = 79
        header = f' Compound {self.name} '
        pad_len_l = ( max_len - len(header) ) // 2
        pad_len_r = max_len - len(header) - pad_len_l
        header_str = '-'*pad_len_l + header + '-'*pad_len_r

        # make basis
        basis_str = []
        vecs = 'abc'
        for v in range(3):
            basis_str.append( f'{self.basis[v, 0]:< 2.15f} {self.basis[v, 1]:< 2.15f} {self.basis[v, 2]:< 2.15f}  {vecs[v]}scale= 1.d0' )
        basis_str = '\n    '.join(map(str, basis_str))

        # make atomic positions
        tol = 15
        from kkr_tool import key_args
        for arg in key_args:
            if '--tautol=' in arg:
                tol = int(arg.split('=')[1])
        nat = -1.7
        at_pos_list = []
        for typ, pos, i in zip(self.at_list, self.at_pos_cart / self.latt_a, range(self.N_at)):
            pos_t = np.around(pos, decimals=tol) + 0.0
            at_pos_list.append( f'{i+1:2}. {typ:2} type={i+1:<2} nat={nat:3}  tau={pos_t[0]: < 2.{tol}f} {pos_t[1]: < 2.{tol}f} {pos_t[2]: < 2.{tol}f} sl=1   ')
        at_pos_str = '\n'.join(map(str, at_pos_list))

        # make atomic configs
        from database.at_configs import at_conf_dict
        at_types = list(np.sort(np.unique(self.at_list)))
        if 'E' in at_types:
            at_types.remove('E')
            at_types.append('E')

        at_conf_str = []
        for typ in at_types:
            nc, c, nv, v = at_conf_dict[typ][1:]
            conf_str = f'{self.at_list.index(typ)+1:2}. label={typ:<2}  nc={nc:<2}  c={c:<30}  nv={nv:1} v={v:<10} '
            at_conf_str.append(conf_str)
        at_conf_str = '\n'.join(map(str, at_conf_str))

        # make atomic options
        at_opt_str = []
        for i, atom in enumerate(self.at_list):
            opt_str = f'{i+1:2}. atom={atom:<2}  type={i+1:<2}  fix=F  lmax=3  lmaxv=6  conc=1.0  mtz=T  sort={i+1}'
            at_opt_str.append(opt_str)
        at_opt_str = '\n'.join(map(str, at_opt_str))

        # make_potentials
        at_pot_str = []
        for i, at_name in enumerate(self.at_list):
            r = self.atoms[i].output_radius*au
            tmp = f'{i+1:2}. {at_name:<2} type={i+1:<2}  np=1001  r1=1.0E-05  rnp={r: < 2.6f}  pfile={at_name}_{i+1}.pot'
            at_pot_str.append(tmp)
        at_pot_str = '\n'.join(map(str, at_pot_str))

        # make file.str
        from read_func.func import load
        from kkr_tool import init_dir
        with open(f'{init_dir}/tool_templates/file.str.temp', 'r') as f:
            temp_file = f.read()

        temp_file = temp_file.\
        replace('!atomic positions!', at_pos_str).\
        replace('!atomic configurations!', at_conf_str).\
        replace('!atomic options!', at_opt_str).\
        replace('!potentials!', at_pot_str).\
        replace('!alat!', str(round(self.latt_a*au, 15))).\
        replace('!basis!', basis_str).\
        replace('!header!', header_str)
        with open('file.str', 'w') as f:
            f.write(temp_file)
        print('Saved.\n')