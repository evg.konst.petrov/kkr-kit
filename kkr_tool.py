#!/usr/bin/env python3
import sys
import os
from __main__ import tool_arg
print('Welcome to kkr.tool! I hope that it will help to make your life a bit easier. ')
cwd = os.getcwd()
init_dir = os.path.dirname(os.path.realpath(__file__))

key_args = []
target = ''

if len(sys.argv) > 1:
    for arg in sys.argv:
        if arg not in os.listdir() and arg[0] == '-':
            key_args.append(arg)
        elif arg in os.listdir():
            target = arg

# ############################################
if tool_arg == 'kmesh2d':
    print('Trying to make a 2D k-point mesh...')
    from tool_func import kmesh2d
    kmesh2d.run()
elif tool_arg == 'poscar2kkr':
    from tool_func import poscar2kkr
    poscar2kkr.run()
elif tool_arg == 'kpoints2kkr':
    from tool_func import kpoints2kkr
    kpoints2kkr.run()
elif tool_arg == 'bz':
    from tool_func import bz
    bz.run()
elif tool_arg == 'cell':
    from tool_func.cell_spheres import CellSpheres
    cell = CellSpheres(target)
    cell.save()

    from tool_func.draw_cell import DrawCell
    DrawCell(cell)

print('Done.')
