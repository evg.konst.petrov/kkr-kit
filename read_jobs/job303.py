#!/usr/bin/env python3
import numpy as np
import os
import sys
from read_func.func import *


def run():
    from kkr_read import key_args
    if '-2d' in key_args:
        print('2D k-point grid is detected.')
        bloch_2d()
    else:
        regular_bloch()


def regular_bloch():
    from kkr_read import key_args, inp, out_dir
    from read_func.bloch import Bloch

    bloch = Bloch()         # get all necessary data
    os.chdir(out_dir)      # go to proper dir

    # save all this
    bsf_file = '_data_BSF'
    kpath_file = '_data_kpath'
    kpoints_file = '_data_kpoints'
    egrid_file = '_data_egrid'
    np.savez_compressed(kpath_file, bloch.kpath)
    np.savez_compressed(kpoints_file, bloch.kpoints)
    np.savez_compressed(egrid_file, bloch.egrid)
    np.savez_compressed(bsf_file, bloch.bsf)

    # dict to write info.py
    info_dict = {
        'name': inp.name,
        'plot_name': f'{inp.name}_job={inp.job}',
        'bsf_file': bsf_file + '.npz',
        'egrid_file': egrid_file + '.npz',
        'kpoints_file': kpoints_file + '.npz',
        'kpath_file': kpath_file + '.npz',
        'nep': bloch.nep,
        'emin': bloch.emin - inp.efermi,
        'emax': bloch.emax - inp.efermi,
        'efermi': inp.efermi,
        'knames': bloch.knames,
        'kindex': bloch.kindex
    }

    # dict to write proper templates
    templates_dict = {
        'job303_bsf.py': '_BSF.py',
        # 'job303_bsf+dos.py': '_bsf+dos.py'
    }

    # TODO
    if bloch.nspin == 2:  # is spin-polarized
        templates_dict.update({'job303_bsf_Sz_pol.py': '_BSF_Sz.py'})
    # TODO add non-col spin
    # TODO add orbital composition
    # TODO add site-resolved orbital composition

    # save templates
    save_templates(templates_dict)
    # write additional python script with useful info for drawing
    write_info(info_dict)

    # delete massive variables
    del(bloch)


def bloch_2d():
    from kkr_read import key_args, inp, out_dir
    from read_func.bloch import Bloch

    bloch = Bloch()         # get all necessary data
    os.chdir(out_dir)      # go to proper dir

    # save all this
    bsf_file = '_data_BSF'
    kpath_file = '_data_kpath'
    kpoints_file = '_data_kpoints'
    egrid_file = '_data_egrid'
    np.savez_compressed(bsf_file, bloch.bsf_2d)
    np.savez_compressed(kpath_file, bloch.kpath_2d)
    np.savez_compressed(kpoints_file, bloch.kpoints)
    np.savez_compressed(egrid_file, bloch.egrid)

    # dict to write info.py
    info_dict = {
        'name': inp.name,
        'plot_name': f'{inp.name}_job={inp.job}',
        'bsf_file': bsf_file + '.npz',
        'egrid_file': egrid_file + '.npz',
        'kpoints_file': kpoints_file + '.npz',
        'kpath_file': kpath_file + '.npz',
        'nep': bloch.nep,
        'emin': bloch.emin - inp.efermi,
        'emax': bloch.emax - inp.efermi,
        'efermi': inp.efermi,
    }

    # dict to write proper templates
    templates_dict = {
        'job303_bsf_2d.py': '_2dBSF.py',
        # 'job303_bsf+dos.py': '_bsf+dos.py'
    }

    # TODO
    if bloch.nspin == 2:  # is spin-polarized
        templates_dict.update({'job303_bsf_Sz_pol_2d.py': '_2dBSF_Sz.py'})
    # TODO add non-col spin
    # TODO add orbital composition
    # TODO add site-resolved orbital composition

    # save templates
    save_templates(templates_dict)
    # write additional python script with useful info for drawing
    write_info(info_dict)

    # delete massive variables
    del(bloch)
