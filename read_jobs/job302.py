#!/usr/bin/env python3
import numpy as np
import os
from read_func.func import *

def run():
    if True:
        regular_dos()


def regular_dos():
    from kkr_read import key_args, inp, out_dir, if_curr_dir, cwd, structure
    from read_func.dos import Dos
    ### get all necessary data
    dos = Dos()

    # go to proper dir
    os.chdir(out_dir)

    # save all this
    dos_tot_file = dos.out_names['dos_tot']
    dos_atom_file = dos.out_names['dos_atom']
    egrid_file = dos.out_names['egrid']
    np.savez_compressed(dos_tot_file, dos.dos_tot)
    np.savez_compressed(dos_atom_file, dos.dos_atom)
    np.savez_compressed(egrid_file, dos.egrid)
    
    # write additional python script with useful info for drawing
    info_dict = {
            'name': structure.name, 
            'plot_name': f'{structure.name}_job={inp.job}',
            'dos_tot_file': dos_tot_file + '.npz',
            'dos_atom_file': dos_atom_file + '.npz',
            'egrid_file': egrid_file + '.npz',
            'nep': dos.nep, 
            'emin': (dos.emin - dos.efermi), 
            'emax': (dos.emax - dos.efermi),
            'efermi': inp.efermi,
            }

    templates_dict = {
        'job302_dos_tot.py': '_DOS_tot.py'
        # 'job303_bsf+dos.py': '_bsf+dos.py'
    }


    if dos.nspin == 2: # if spin-polarized
        templates_dict.update(
            {'job302_dos_tot_Sz_pol.py': '_DOS_Sz.py'})
    elif dos.nspin == 3:
        templates_dict.update(
            {'job302_dos_tot_Sz_ncol.py': '_DOS_Sz.py',
            'job302_dos_tot_Sx_ncol.py': '_DOS_Sx.py',
            'job302_dos_tot_Sy_ncol.py': '_DOS_Sy.py'})
    #TODO add non-col spin
    #TODO add orbital composition
    #TODO add site-resolved orbital composition

    save_templates( templates_dict )
    write_info( info_dict )

    # delete massive variables
    del(dos)