#!/usr/bin/env python3
import numpy as np
import os
from read_func.func import *

def run():
    if True:
        regular_magnon()


def regular_magnon():
    from kkr_read import key_args, inp, out_dir, cwd
    from read_func.magnon import Magnon
    ### get all necessary data
    mag = Magnon()

    # go to proper dir
    os.chdir(out_dir)

    # save all this
    kpath_file = mag.out_names['kpath']
    magnon_file = mag.out_names['magnon']
    jq_file = mag.out_names['jq']
    jrs_file = mag.out_names['jrs']
    np.savez_compressed(kpath_file, mag.kpath)
    np.savez_compressed(magnon_file, mag.magnon)
    np.savez_compressed(jq_file, mag.jq)
    # np.savez_compressed(jrs_file, mag.jrs)
    
    # write additional python script with useful info for drawing
    info_dict = {
            'name': inp.name, 
            'plot_name': f'{inp.name}_job={inp.job}',
            'kpath_file': f'{kpath_file}.npz',
            'magnon_file': f'{magnon_file}.npz',
            'jq_file': f'{jq_file}.npz',
            'jrs_file': f'{jrs_file}.npz',
            'knames': mag.knames,
            'kindex': mag.kindex,
            'ryd': 13.6056980659, 
            }

    templates_dict = {
        'job312_mag_jq_jrs.py': '_mag+jq+jrs.py',
        'job312_mag_jq.py': '_mag+jq.py'
    }

    save_templates( templates_dict )
    write_info( info_dict )

    # delete massive variables
    del(mag)