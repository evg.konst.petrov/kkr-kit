#!/usr/bin/env python3
import matplotlib as mm
import matplotlib.pyplot as pp
import numpy as np
from matplotlib.gridspec import GridSpec
from info import *
# from kkr_plot import *      # all useful is there

# name of the resulting file
out_name = f'{plot_name}_mag+jq+jrs.pdf'


# page size
Sx = 8
Sy = 8

# set figure margins (page size units): left, right, bottom, top
lm = 0.08
rm = 1 - 0.2/Sx
bm = 0.05
tm = 0.92

color_grid = 'grey'

# create figure, subplot grid and axes
fig = pp.figure(figsize=(Sx, Sy)) # create figure and set its size
gs = GridSpec(2, 2, left=lm, bottom=bm, right=rm, top=tm,
              wspace=0.25, hspace=0.12) # create plot grid

# create axes
ax_magnon = pp.subplot(gs[0, 0])
ax_jq = pp.subplot(gs[1, 0])
ax_jrs = pp.subplot(gs[:, 1])
axes = [ax_magnon, ax_jq, ax_jrs]

# load files
kpath = np.load(kpath_file)['arr_0']
magnon = np.load(magnon_file)['arr_0']
jq = np.load(jq_file)['arr_0']

for ax in axes:
    ax.axhline(0, color='grey', lw=1, zorder=1)
    ax.set_ylabel('$E$, meV')

ax_magnon.plot(kpath, magnon*ryd, lw=1)
ax_jq.plot(kpath, jq*1000*ryd, lw=1, zorder=10)

for ax in axes[:2]:
    ax.set_xticks(kpath[kindex])
    ax.grid(axis='x', lw=0.5, color=color_grid)
    ax.tick_params('x', length=0, width=0, which='both')

ax_magnon.set_xticklabels([])
ax_jq.set_xticklabels(knames)

ax_magnon.set_title('Magnon spectrum', loc='left', fontsize=10)
ax_jq.set_title('J(q) spectrum', loc='left', fontsize=10)
ax_jrs.set_title('J(r, site) spectrum', loc='left', fontsize=10)





fig.suptitle(plot_name)
# save
fig.savefig(out_name, dpi=300)

