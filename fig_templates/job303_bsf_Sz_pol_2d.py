#!/usr/bin/env python3
from matplotlib.colors import LinearSegmentedColormap as LSC
import matplotlib as mm
import matplotlib.pyplot as pp
import numpy as np
from matplotlib.gridspec import GridSpec
from info import *

# name of the resulting file
out_name = f'{plot_name}_2d_spin.pdf'

# axes limits
emin = emin
emax = emax

# scaling factor
fat = 2

# page size
Sx = 8
Sy = 8
# set figure margins (page size units): left, right, bottom, top
lm = 0.08
rm = 0.98
bm = 0.05
tm = 0.95

# PLOT GRID
# create figure and set its size
fig = pp.figure(figsize=(Sx, Sy))
gs = GridSpec(1, 1, left=lm, bottom=bm, right=rm, top=tm,
              wspace=0.0, hspace=0.0)           # create plot grid
ax = pp.subplot(gs[0])

# BLOCH and k-points
kpath = np.load(kpath_file)['arr_0']
kxs = kpath[0]
kys = kpath[1]
nkx = len(kxs)
nky = len(kys)

BSF = np.load(bsf_file)['arr_0']
BSF = np.tanh(BSF / (np.max(BSF)/(3*fat)))

# create two transparent colormaps: UP and DOWN
cmaps = [LSC.from_list('', ((1, 1, 1, 0), (1, 0, 0, 0.7))),
         LSC.from_list('', ((1, 1, 1, 0), (0, 0, 1, 0.7)))]

for i, j in enumerate(range(1, 3)):
    ax.pcolormesh(kxs, kys,
                  np.reshape(BSF[j], (nkx, nky), order='F'),
                  cmap=cmaps[i], vmin=0, rasterized=True)

# adjust axes
ax.set_xlim([np.min(kxs), np.max(kxs)])
ax.set_ylim([np.min(kys), np.max(kys)])

# add labels and titles
ax.set_xlabel('$k_x$')
ax.set_ylabel('$k_y$')
ax.set_title(name, loc='left')

# save
fig.savefig(out_name, dpi=600)
