#!/usr/bin/env python3
import matplotlib as mm
import matplotlib.pyplot as pp
import numpy as np
from matplotlib.gridspec import GridSpec
from info import *

# name of the resulting file
out_name = f'{plot_name}_spin.pdf'

# axes limits
kmin = 0            # 0 = first, -1 = last
kmax = -1           # 0 = first, -1 = last
emin = emin
emax = emax

# scaling factor
fat = 2

# page size
Sx = 8
Sy = 4.5
# set figure margins (page size units): left, right, bottom, top
lm = 0.8/Sx
rm = 1 - 0.2/Sx
bm = 0.05
tm = 0.95

### PLOT GRID
fig = pp.figure(figsize=(Sx, Sy))               # create figure and set its size
gs = GridSpec(1, 1, left=lm, bottom=bm, right=rm, top=tm,
            wspace=0.0, hspace=0.0)           # create plot grid
ax = pp.subplot(gs[0])

# BLOCH
egrid = (np.load(egrid_file)['arr_0'] - efermi)
kpath = np.load(kpath_file)['arr_0']
BSF = np.load(bsf_file)['arr_0']
BSF = np.tanh( BSF / (np.max(BSF)/(3*fat)))

# create two transparent colormaps
from matplotlib.colors import LinearSegmentedColormap as LSC
cmaps = [ LSC.from_list('', ( (1,1,1,0), (1,0,0,0.7) )),
          LSC.from_list('', ( (1,1,1,0), (0,0,1,0.7) ))]

#plot spins up and down
for i, j in enumerate(range(1,3)):
    ax.pcolormesh(kpath, egrid,
                BSF[j], cmap=cmaps[i], vmin=0, rasterized=True)


ax.axhline(0, color='grey', lw=1, zorder=1)           # plot fermi level

# adjust axes
ax.set_xlim([kpath[kmin], kpath[kmax]])
ax.set_ylim([emin, emax])
ax.tick_params('x', length=0, which='both')
ax.grid(True, axis='x', color='grey')

# add labels and titles
ax.set_ylabel('$E - E_F$, eV')
ax.set_xticklabels(knames)
ax.set_xticks(kpath[kindex])
ax.set_title(name, loc='left')

# save
fig.savefig(out_name, dpi=600)

