#!/usr/bin/env python3
import matplotlib as mm
import matplotlib.pyplot as pp
import numpy as np
from matplotlib.gridspec import GridSpec
from info import *

# name of the resulting file
out_name = f'{plot_name}_tot.pdf'

# axes limits
kmin = 0            # 0 = first, -1 = last
kmax = -1           # 0 = first, -1 = last
emin = emin
emax = emax

# page size
Sx = 4.5
Sy = 3
# set figure margins (page size units): left, right, bottom, top
lm = 0.75/Sx
rm = 0.95
bm = 0.15
tm = 0.9

### DO STUFF
# create figure, subplot grid and axes
fig = pp.figure(figsize=(Sx, Sy)) # create figure and set its size
gs = GridSpec(1, 1, left=lm, bottom=bm, right=rm, top=tm,
              wspace=0.0, hspace=0.0) # create plot grid
ax = pp.subplot(gs[0])

# BLOCH
egrid = (np.load(egrid_file)['arr_0'] - efermi)*ryd
DOS = np.load(dos_file)['arr_0']
ax.fill_between(egrid, DOS[1], color='black',alpha=0.7)
ax.axvline(0, color='grey', lw=1, zorder=1)           # plot fermi level

# adjust axes
ax.set_xlim([emin, emax])
ax.tick_params('x', length=0, which='both')

# add labels and titles
ax.set_xlabel('$E - E_\mathrm{F}$ (eV)')
ax.set_ylabel('total DOS (states/eV)')
ax.set_title(name, loc='left')

# save
fig.savefig(out_name, dpi=300)

