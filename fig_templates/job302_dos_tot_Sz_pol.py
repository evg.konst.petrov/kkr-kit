#!/usr/bin/env python3
import matplotlib as mm
import matplotlib.pyplot as pp
import numpy as np
from matplotlib.gridspec import GridSpec
from info import *

# name of the resulting file
out_name = f'{plot_name}_Sz.pdf'

# axes limits
kmin = 0            # 0 = first, -1 = last
kmax = -1           # 0 = first, -1 = last
emin = emin
emax = emax

# page size
Sx = 4.5
Sy = 4
# set figure margins (page size units): left, right, bottom, top
lm = 0.75/Sx
rm = 0.95
bm = 0.10
tm = 0.94

### DO STUFF
# create figure, subplot grid and axes
fig = pp.figure(figsize=(Sx, Sy)) # create figure and set its size
gs = GridSpec(1, 1, left=lm, bottom=bm, right=rm, top=tm,
              wspace=0.0, hspace=0.0) # create plot grid
ax = pp.subplot(gs[0])

# BLOCH
egrid = (np.load(egrid_file)['arr_0'] - efermi)*ryd
DOS = np.load(dos_file)['arr_0']
ax.fill_between(egrid, DOS[3], color='red', alpha=0.7)
ax.fill_between(egrid,-DOS[5], color='royalblue', alpha=0.7)
ax.axvline(0, color='grey', lw=1, zorder=1)           # plot fermi level

# adjust axes
ax.set_xlim([emin, emax])
ax.tick_params('x', length=0, which='both')

# add labels and titles
ax.set_xlabel('$E - E_\mathrm{F}$ (eV)')
ax.set_ylabel('DOS (states/eV)')
ax.set_title(name, loc='left')

# spin labels, note \u is misinterpreted in python, therefore "\\"uparrow"
ax.text(0.9,0.04,'spin $\downarrow$', fontsize=12,
    horizontalalignment='center', verticalalignment='center', transform=ax.transAxes)
ax.text(0.9,0.96,'spin $\\uparrow$', fontsize=12, 
    horizontalalignment='center', verticalalignment='center', transform=ax.transAxes)

# save
fig.savefig(out_name, dpi=300)

